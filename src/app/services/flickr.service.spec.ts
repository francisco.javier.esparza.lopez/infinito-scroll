import { TestBed } from '@angular/core/testing';

import { FlickrService } from './flickr.service';
import { HttpClientTestingModule } from '@angular/common/http/testing'

describe('FlickrService', () => {
  beforeEach(() => TestBed.configureTestingModule({
    imports: [HttpClientTestingModule]

  }));

  //probando conexion a flicker y paramametros que puedan obtener datos
  it('Creacion exitosa y verifica conexion http al webservice', () => {
    const service: FlickrService = TestBed.get(FlickrService);
    expect(service).toBeTruthy();
  });
});
