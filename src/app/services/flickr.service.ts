import { Injectable, SystemJsNgModuleLoader } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';

export interface FlickrPhoto {
  farm: string;
  id: string;
  secret: string;
  server: string;
  title: string;
}

export interface FlickrOutput {
  photos: {
    photo: FlickrPhoto[];
  };
}

@Injectable({
  providedIn: 'root'
})
export class FlickrService {
  prevKeyword: string;
  currPage = 1;

  constructor(private http: HttpClient) { }

  search_keyword(keyword: string) {
    if (this.prevKeyword === keyword) {
      this.currPage++;
    } else {
      this.currPage = 1;
    }
    this.prevKeyword = keyword;
    const url = 'https://www.flickr.com/services/rest/?method=flickr.photos.search&';
    const params = `api_key=${environment.flickr.key}&text=${keyword}&format=json&nojsoncallback=1&per_page=12&page=${this.currPage}`;

    return this.http.get(url + params).pipe(map((res: FlickrOutput) => {
      const urlArr = [];
   // tracking parameters    console.log(url+params);
      res.photos.photo.forEach((ph: FlickrPhoto) => {
// Recovery the last stage in memory  from favoritos
      let favoritoVar = localStorage.getItem('favoritos'+ph.id) != null? JSON.parse(localStorage.getItem('favoritos'+ph.id)):false;
   /* to check storage  
      console.log(localStorage.getItem('favoritos'+ph.id));
      console.log(favoritoVar);
      */
//console.log (favoritoVar); manual testing done
        const photoObj = {
          id : ph.id, //to make favoritos control 
          url: `https://farm${ph.farm}.staticflickr.com/${ph.server}/${ph.id}_${ph.secret}`,
          title: ph.title,
          favorito:favoritoVar
        };
        urlArr.push(photoObj);
      });
      return urlArr;
    }));
  }
}
