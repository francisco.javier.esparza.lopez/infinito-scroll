import { Component, OnInit } from '@angular/core';
import { FlickrService } from '../services/flickr.service';

@Component({
  selector: 'app-search-images',
  templateUrl: './search-images.component.html',
  styleUrls: ['./search-images.component.css']
})
export class SearchImagesComponent implements OnInit {
  imagenes = [];
  favorites = [];
  keyword: string ="Aguascalientes";

  constructor(private flickrService: FlickrService) {
    // to show up Aguascalientes
    this.flickrService.search_keyword(this.keyword)
    .toPromise()
    .then(res => {
      this.imagenes = res;
    });
  }

  ngOnInit() {
   
  }

  search(event: any) {
    this.keyword = event.target.value.toLowerCase();
    if (this.keyword && this.keyword.length > 0) {
      this.flickrService.search_keyword(this.keyword)
        .toPromise()
        .then(res => {
          this.imagenes = res;
        });
    }
  }

  onScroll() {
    if (this.keyword && this.keyword.length > 0) {
      this.flickrService.search_keyword(this.keyword)
        .toPromise()
        .then(res => {
          this.imagenes = this.imagenes.concat(res);
        });
    }

  }

  onSelect(imagen: any): void {
    imagen.favorito = !imagen.favorito;    

    //to keep in memory the current state
    localStorage.setItem('favoritos'+imagen.id,imagen.favorito);
  }

  readLocalStorageValue(key) {
    return localStorage.getItem(key);
  }

}
