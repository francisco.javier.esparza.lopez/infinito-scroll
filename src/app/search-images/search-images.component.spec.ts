import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SearchImagesComponent } from './search-images.component';
import { HttpClientTestingModule } from '@angular/common/http/testing'

describe('SearchImagesComponent', () => {
  let component: SearchImagesComponent;
  let fixture: ComponentFixture<SearchImagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule]
       ,
      declarations: [ SearchImagesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SearchImagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

// verifica que se pueda crear los componentes. 
  it('Conponente creado y validado', () => {
    expect(component).toBeTruthy();
  });
});
