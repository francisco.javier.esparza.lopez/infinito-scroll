#Front-end Homework Assignment

## Preface

At Perfection42, we strive to write clean and simple code, covered with unit tests, and easy to maintain. Our front-end developers should be knowledgeable in latest HTML, CSS or Javascript things. 

[animated](https://5a2583d7dd16c25cb2e8-358d15e499fca729302e63598be13736.ssl.cf3.rackcdn.com/frontend/hw-example-animated.gif)


Based on the provided screenshots, make a simple `infinite-scroll` capable web application that allows for the user to browse items and favourite them.

Aditional features by developers:
This example include a search model 


### Requirements
* You are to use [Flickr API](https://www.flickr.com/services/api/flickr.photos.getRecent.html) (recommended) or a similar API for data retrieval. If you choose another API, make sure it supports paginated results, unique images with some metadata and a way for us to access it.
* Design should be recreated as closely as possible, including item hover state.
* Responsive design (with at least three breakpoints — Desktop, Tablet, Phone).
* Infinite scroll - it's a concept where additional data is loaded when user scrolls down the screen.
* A possibility to favourite an item (favourites should not be lost on page reload).
* It is preferred to use `React`, but you can write vanilla JS code as well (no other libraries/frameworks, though).
* You are only allowed to use `react`, `react-dom` and your choice of any development-environment specific libraries (testing tools, babel, etc). All other 3rd-party libraries are forbidden (`Redux`, `lodash`, `jQuery`, `axios`, `bootstrap`, etc).


## Installation 

In the project folder, type:
npm install

## Usage

In the project folder, type:
ng serve

Then open your browser and go to:
http://localhost:4200/
