/******/ (function(modules) { // webpackBootstrap
/******/ 	// install a JSONP callback for chunk loading
/******/ 	function webpackJsonpCallback(data) {
/******/ 		var chunkIds = data[0];
/******/ 		var moreModules = data[1];
/******/ 		var executeModules = data[2];
/******/
/******/ 		// add "moreModules" to the modules object,
/******/ 		// then flag all "chunkIds" as loaded and fire callback
/******/ 		var moduleId, chunkId, i = 0, resolves = [];
/******/ 		for(;i < chunkIds.length; i++) {
/******/ 			chunkId = chunkIds[i];
/******/ 			if(Object.prototype.hasOwnProperty.call(installedChunks, chunkId) && installedChunks[chunkId]) {
/******/ 				resolves.push(installedChunks[chunkId][0]);
/******/ 			}
/******/ 			installedChunks[chunkId] = 0;
/******/ 		}
/******/ 		for(moduleId in moreModules) {
/******/ 			if(Object.prototype.hasOwnProperty.call(moreModules, moduleId)) {
/******/ 				modules[moduleId] = moreModules[moduleId];
/******/ 			}
/******/ 		}
/******/ 		if(parentJsonpFunction) parentJsonpFunction(data);
/******/
/******/ 		while(resolves.length) {
/******/ 			resolves.shift()();
/******/ 		}
/******/
/******/ 		// add entry modules from loaded chunk to deferred list
/******/ 		deferredModules.push.apply(deferredModules, executeModules || []);
/******/
/******/ 		// run deferred modules when all chunks ready
/******/ 		return checkDeferredModules();
/******/ 	};
/******/ 	function checkDeferredModules() {
/******/ 		var result;
/******/ 		for(var i = 0; i < deferredModules.length; i++) {
/******/ 			var deferredModule = deferredModules[i];
/******/ 			var fulfilled = true;
/******/ 			for(var j = 1; j < deferredModule.length; j++) {
/******/ 				var depId = deferredModule[j];
/******/ 				if(installedChunks[depId] !== 0) fulfilled = false;
/******/ 			}
/******/ 			if(fulfilled) {
/******/ 				deferredModules.splice(i--, 1);
/******/ 				result = __webpack_require__(__webpack_require__.s = deferredModule[0]);
/******/ 			}
/******/ 		}
/******/
/******/ 		return result;
/******/ 	}
/******/
/******/ 	// The module cache
/******/ 	var installedModules = {};
/******/
/******/ 	// object to store loaded and loading chunks
/******/ 	// undefined = chunk not loaded, null = chunk preloaded/prefetched
/******/ 	// Promise = chunk loading, 0 = chunk loaded
/******/ 	var installedChunks = {
/******/ 		"main": 0
/******/ 	};
/******/
/******/ 	var deferredModules = [];
/******/
/******/ 	// The require function
/******/ 	function __webpack_require__(moduleId) {
/******/
/******/ 		// Check if module is in cache
/******/ 		if(installedModules[moduleId]) {
/******/ 			return installedModules[moduleId].exports;
/******/ 		}
/******/ 		// Create a new module (and put it into the cache)
/******/ 		var module = installedModules[moduleId] = {
/******/ 			i: moduleId,
/******/ 			l: false,
/******/ 			exports: {}
/******/ 		};
/******/
/******/ 		// Execute the module function
/******/ 		modules[moduleId].call(module.exports, module, module.exports, __webpack_require__);
/******/
/******/ 		// Flag the module as loaded
/******/ 		module.l = true;
/******/
/******/ 		// Return the exports of the module
/******/ 		return module.exports;
/******/ 	}
/******/
/******/
/******/ 	// expose the modules object (__webpack_modules__)
/******/ 	__webpack_require__.m = modules;
/******/
/******/ 	// expose the module cache
/******/ 	__webpack_require__.c = installedModules;
/******/
/******/ 	// define getter function for harmony exports
/******/ 	__webpack_require__.d = function(exports, name, getter) {
/******/ 		if(!__webpack_require__.o(exports, name)) {
/******/ 			Object.defineProperty(exports, name, { enumerable: true, get: getter });
/******/ 		}
/******/ 	};
/******/
/******/ 	// define __esModule on exports
/******/ 	__webpack_require__.r = function(exports) {
/******/ 		if(typeof Symbol !== 'undefined' && Symbol.toStringTag) {
/******/ 			Object.defineProperty(exports, Symbol.toStringTag, { value: 'Module' });
/******/ 		}
/******/ 		Object.defineProperty(exports, '__esModule', { value: true });
/******/ 	};
/******/
/******/ 	// create a fake namespace object
/******/ 	// mode & 1: value is a module id, require it
/******/ 	// mode & 2: merge all properties of value into the ns
/******/ 	// mode & 4: return value when already ns object
/******/ 	// mode & 8|1: behave like require
/******/ 	__webpack_require__.t = function(value, mode) {
/******/ 		if(mode & 1) value = __webpack_require__(value);
/******/ 		if(mode & 8) return value;
/******/ 		if((mode & 4) && typeof value === 'object' && value && value.__esModule) return value;
/******/ 		var ns = Object.create(null);
/******/ 		__webpack_require__.r(ns);
/******/ 		Object.defineProperty(ns, 'default', { enumerable: true, value: value });
/******/ 		if(mode & 2 && typeof value != 'string') for(var key in value) __webpack_require__.d(ns, key, function(key) { return value[key]; }.bind(null, key));
/******/ 		return ns;
/******/ 	};
/******/
/******/ 	// getDefaultExport function for compatibility with non-harmony modules
/******/ 	__webpack_require__.n = function(module) {
/******/ 		var getter = module && module.__esModule ?
/******/ 			function getDefault() { return module['default']; } :
/******/ 			function getModuleExports() { return module; };
/******/ 		__webpack_require__.d(getter, 'a', getter);
/******/ 		return getter;
/******/ 	};
/******/
/******/ 	// Object.prototype.hasOwnProperty.call
/******/ 	__webpack_require__.o = function(object, property) { return Object.prototype.hasOwnProperty.call(object, property); };
/******/
/******/ 	// __webpack_public_path__
/******/ 	__webpack_require__.p = "/_karma_webpack_/";
/******/
/******/ 	var jsonpArray = window["webpackJsonp"] = window["webpackJsonp"] || [];
/******/ 	var oldJsonpFunction = jsonpArray.push.bind(jsonpArray);
/******/ 	jsonpArray.push = webpackJsonpCallback;
/******/ 	jsonpArray = jsonpArray.slice();
/******/ 	for(var i = 0; i < jsonpArray.length; i++) webpackJsonpCallback(jsonpArray[i]);
/******/ 	var parentJsonpFunction = oldJsonpFunction;
/******/
/******/
/******/ 	// add entry module to deferred list
/******/ 	deferredModules.push(["./src/test.ts","vendor"]);
/******/ 	// run deferred modules when ready
/******/ 	return checkDeferredModules();
/******/ })
/************************************************************************/
/******/ ({

/***/ "./$$_lazy_route_resource lazy recursive":
/*!******************************************************!*\
  !*** ./$$_lazy_route_resource lazy namespace object ***!
  \******************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html":
/*!**************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html ***!
  \**************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("<app-search-images></app-search-images>\n");

/***/ }),

/***/ "./node_modules/raw-loader/dist/cjs.js!./src/app/search-images/search-images.component.html":
/*!**************************************************************************************************!*\
  !*** ./node_modules/raw-loader/dist/cjs.js!./src/app/search-images/search-images.component.html ***!
  \**************************************************************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n\n  <div style=\"text-align: center;\">\n    <input type=\"search\" class=\"form-control\" (keyup)=\"search($event)\" placeholder=\"Search images\" value=\"Aguascalientes\">\n  </div>\n\n  <div class=\"caja\">\n    <div class=\"row\">\n      <div *ngFor=\"let imagen of imagenes\" (click)=\"onSelect(imagen)\">\n        <div class=\"column\">\n          <img src=\"{{imagen.url}}_m.jpg\" title=\"{{imagen.title}}\">          \n          \n            <div class=\"text\" *ngIf=\"imagen.favorito\" > \n              <h1>Selected</h1>\n          </div>    \n        </div>\n      </div>\n    </div>\n    <div class=\"search-results\" infiniteScroll [infiniteScrollDistance]=\"2\" [infiniteScrollThrottle]=\"50\"\n      (scrolled)=\"onScroll()\">\n    </div>\n  </div>\n\n");

/***/ }),

/***/ "./node_modules/webpack/buildin/global.js":
/*!***********************************!*\
  !*** (webpack)/buildin/global.js ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

var g;

// This works in non-strict mode
g = (function() {
	return this;
})();

try {
	// This works if eval is allowed (see CSP)
	g = g || new Function("return this")();
} catch (e) {
	// This works if the window reference is available
	if (typeof window === "object") g = window;
}

// g can still be undefined, but nothing to do about it...
// We return undefined, instead of nothing here, so it's
// easier to handle this case. if(!global) { ...}

module.exports = g;


/***/ }),

/***/ "./src sync recursive \\.spec\\.ts$":
/*!******************************!*\
  !*** ./src sync \.spec\.ts$ ***!
  \******************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

var map = {
	"./app/app.component.spec.ts": "./src/app/app.component.spec.ts",
	"./app/search-images/search-images.component.spec.ts": "./src/app/search-images/search-images.component.spec.ts",
	"./app/services/flickr.service.spec.ts": "./src/app/services/flickr.service.spec.ts"
};


function webpackContext(req) {
	var id = webpackContextResolve(req);
	return __webpack_require__(id);
}
function webpackContextResolve(req) {
	if(!__webpack_require__.o(map, req)) {
		var e = new Error("Cannot find module '" + req + "'");
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	}
	return map[req];
}
webpackContext.keys = function webpackContextKeys() {
	return Object.keys(map);
};
webpackContext.resolve = webpackContextResolve;
module.exports = webpackContext;
webpackContext.id = "./src sync recursive \\.spec\\.ts$";

/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IiIsImZpbGUiOiJzcmMvYXBwL2FwcC5jb21wb25lbnQuY3NzIn0= */");

/***/ }),

/***/ "./src/app/app.component.spec.ts":
/*!***************************************!*\
  !*** ./src/app/app.component.spec.ts ***!
  \***************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core_testing__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core/testing */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/testing.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");


describe('AppComponent', () => {
    beforeEach(Object(_angular_core_testing__WEBPACK_IMPORTED_MODULE_0__["async"])(() => {
        _angular_core_testing__WEBPACK_IMPORTED_MODULE_0__["TestBed"].configureTestingModule({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_1__["AppComponent"]
            ],
        }).compileComponents();
    }));
    it('Creacion del componente', () => {
        const fixture = _angular_core_testing__WEBPACK_IMPORTED_MODULE_0__["TestBed"].createComponent(_app_component__WEBPACK_IMPORTED_MODULE_1__["AppComponent"]);
        const app = fixture.debugElement.componentInstance;
        expect(app).toBeTruthy();
    });
    it('Creacion solo de el marco de trabajo', () => {
        //because all will be used in the next components
        const fixture = _angular_core_testing__WEBPACK_IMPORTED_MODULE_0__["TestBed"].createComponent(_app_component__WEBPACK_IMPORTED_MODULE_1__["AppComponent"]);
        const app = fixture.debugElement.componentInstance;
        expect(app.title).toEqual('Infinite-scroll');
    });
});


/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");


let AppComponent = class AppComponent {
    constructor() {
        //inicializa el nombre del projecto. 
        this.title = 'Infinite-scroll';
    }
};
AppComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-root',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./app.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/app.component.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")).default]
    })
], AppComponent);



/***/ }),

/***/ "./src/app/search-images/search-images.component.css":
/*!***********************************************************!*\
  !*** ./src/app/search-images/search-images.component.css ***!
  \***********************************************************/
/*! exports provided: default */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony default export */ __webpack_exports__["default"] = ("*{\n  box-sizing: border-box;\n}\n\n.column{\n  position: relative;\n  display: inline-block; /* Make the width of box same as image */\n  float: left;\n  width: 30%;\n  \n  margin: 1rem;\n}\n\n.column img {\n  border-radius: 20px;\n  border: 3px solid lightgray;\n \n}\n\n.column .text{\n  position: absolute;\n  z-index: 999;\n  margin: 0 auto;\n  left: 0;\n  right: 0;        \n  text-align: center;\n  top: 40%; /* Adjust this value to move the positioned div up and down */\n  background: rgba(0, 0, 0, 0.8);\n  font-family: Arial,sans-serif;\n  color: #fff;\n  width: 60%; /* Set the width of the positioned div */\n}\n\n.row:after {\n  content: \"\";\n  display: table;\n  clear: both;\n}\n\n.caja {\n  margin-left: 15%;\n  margin-right: 15%;\n  background-color: #e7e7e7;\n  padding: 50px;\n  color: black;\n  text-align: center;\n  font-size: 14px;\n  box-shadow: 0 1px 3px rgba(0, 0, 0, 0.12), 0 1px 2px rgba(0, 0, 0, 0.24);\n}\n\nimg {\n  -o-object-fit: cover;\n     object-fit: cover;\n  width: 100%;\n  height: 250px;\n}\n\n@media screen and (max-width: 992px) {\n  .column {\n    width: 50%;\n  }\n}\n\n@media screen and (max-width: 600px) {\n  .column {\n    width: 100%;\n  }\n}\n\n/*# sourceMappingURL=data:application/json;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbInNyYy9hcHAvc2VhcmNoLWltYWdlcy9zZWFyY2gtaW1hZ2VzLmNvbXBvbmVudC5jc3MiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IkFBQUE7RUFDRSxzQkFBc0I7QUFDeEI7O0FBRUE7RUFDRSxrQkFBa0I7RUFDbEIscUJBQXFCLEVBQUUsd0NBQXdDO0VBQy9ELFdBQVc7RUFDWCxVQUFVOztFQUVWLFlBQVk7QUFDZDs7QUFDQTtFQUNFLG1CQUFtQjtFQUNuQiwyQkFBMkI7O0FBRTdCOztBQUNBO0VBQ0Usa0JBQWtCO0VBQ2xCLFlBQVk7RUFDWixjQUFjO0VBQ2QsT0FBTztFQUNQLFFBQVE7RUFDUixrQkFBa0I7RUFDbEIsUUFBUSxFQUFFLDZEQUE2RDtFQUN2RSw4QkFBOEI7RUFDOUIsNkJBQTZCO0VBQzdCLFdBQVc7RUFDWCxVQUFVLEVBQUUsd0NBQXdDO0FBQ3REOztBQUVBO0VBQ0UsV0FBVztFQUNYLGNBQWM7RUFDZCxXQUFXO0FBQ2I7O0FBRUE7RUFDRSxnQkFBZ0I7RUFDaEIsaUJBQWlCO0VBQ2pCLHlCQUF5QjtFQUN6QixhQUFhO0VBQ2IsWUFBWTtFQUNaLGtCQUFrQjtFQUNsQixlQUFlO0VBQ2Ysd0VBQXdFO0FBQzFFOztBQUVBO0VBQ0Usb0JBQWlCO0tBQWpCLGlCQUFpQjtFQUNqQixXQUFXO0VBQ1gsYUFBYTtBQUNmOztBQUVBO0VBQ0U7SUFDRSxVQUFVO0VBQ1o7QUFDRjs7QUFFQTtFQUNFO0lBQ0UsV0FBVztFQUNiO0FBQ0YiLCJmaWxlIjoic3JjL2FwcC9zZWFyY2gtaW1hZ2VzL3NlYXJjaC1pbWFnZXMuY29tcG9uZW50LmNzcyIsInNvdXJjZXNDb250ZW50IjpbIip7XG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XG59XG5cbi5jb2x1bW57XG4gIHBvc2l0aW9uOiByZWxhdGl2ZTtcbiAgZGlzcGxheTogaW5saW5lLWJsb2NrOyAvKiBNYWtlIHRoZSB3aWR0aCBvZiBib3ggc2FtZSBhcyBpbWFnZSAqL1xuICBmbG9hdDogbGVmdDtcbiAgd2lkdGg6IDMwJTtcbiAgXG4gIG1hcmdpbjogMXJlbTtcbn1cbi5jb2x1bW4gaW1nIHtcbiAgYm9yZGVyLXJhZGl1czogMjBweDtcbiAgYm9yZGVyOiAzcHggc29saWQgbGlnaHRncmF5O1xuIFxufVxuLmNvbHVtbiAudGV4dHtcbiAgcG9zaXRpb246IGFic29sdXRlO1xuICB6LWluZGV4OiA5OTk7XG4gIG1hcmdpbjogMCBhdXRvO1xuICBsZWZ0OiAwO1xuICByaWdodDogMDsgICAgICAgIFxuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIHRvcDogNDAlOyAvKiBBZGp1c3QgdGhpcyB2YWx1ZSB0byBtb3ZlIHRoZSBwb3NpdGlvbmVkIGRpdiB1cCBhbmQgZG93biAqL1xuICBiYWNrZ3JvdW5kOiByZ2JhKDAsIDAsIDAsIDAuOCk7XG4gIGZvbnQtZmFtaWx5OiBBcmlhbCxzYW5zLXNlcmlmO1xuICBjb2xvcjogI2ZmZjtcbiAgd2lkdGg6IDYwJTsgLyogU2V0IHRoZSB3aWR0aCBvZiB0aGUgcG9zaXRpb25lZCBkaXYgKi9cbn1cblxuLnJvdzphZnRlciB7XG4gIGNvbnRlbnQ6IFwiXCI7XG4gIGRpc3BsYXk6IHRhYmxlO1xuICBjbGVhcjogYm90aDtcbn1cblxuLmNhamEge1xuICBtYXJnaW4tbGVmdDogMTUlO1xuICBtYXJnaW4tcmlnaHQ6IDE1JTtcbiAgYmFja2dyb3VuZC1jb2xvcjogI2U3ZTdlNztcbiAgcGFkZGluZzogNTBweDtcbiAgY29sb3I6IGJsYWNrO1xuICB0ZXh0LWFsaWduOiBjZW50ZXI7XG4gIGZvbnQtc2l6ZTogMTRweDtcbiAgYm94LXNoYWRvdzogMCAxcHggM3B4IHJnYmEoMCwgMCwgMCwgMC4xMiksIDAgMXB4IDJweCByZ2JhKDAsIDAsIDAsIDAuMjQpO1xufVxuXG5pbWcge1xuICBvYmplY3QtZml0OiBjb3ZlcjtcbiAgd2lkdGg6IDEwMCU7XG4gIGhlaWdodDogMjUwcHg7XG59XG5cbkBtZWRpYSBzY3JlZW4gYW5kIChtYXgtd2lkdGg6IDk5MnB4KSB7XG4gIC5jb2x1bW4ge1xuICAgIHdpZHRoOiA1MCU7XG4gIH1cbn1cblxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogNjAwcHgpIHtcbiAgLmNvbHVtbiB7XG4gICAgd2lkdGg6IDEwMCU7XG4gIH1cbn1cbiJdfQ== */");

/***/ }),

/***/ "./src/app/search-images/search-images.component.spec.ts":
/*!***************************************************************!*\
  !*** ./src/app/search-images/search-images.component.spec.ts ***!
  \***************************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core_testing__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core/testing */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/testing.js");
/* harmony import */ var _search_images_component__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./search-images.component */ "./src/app/search-images/search-images.component.ts");
/* harmony import */ var _angular_common_http_testing__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http/testing */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http/testing.js");



describe('SearchImagesComponent', () => {
    let component;
    let fixture;
    beforeEach(Object(_angular_core_testing__WEBPACK_IMPORTED_MODULE_0__["async"])(() => {
        _angular_core_testing__WEBPACK_IMPORTED_MODULE_0__["TestBed"].configureTestingModule({
            imports: [_angular_common_http_testing__WEBPACK_IMPORTED_MODULE_2__["HttpClientTestingModule"]],
            declarations: [_search_images_component__WEBPACK_IMPORTED_MODULE_1__["SearchImagesComponent"]]
        })
            .compileComponents();
    }));
    beforeEach(() => {
        fixture = _angular_core_testing__WEBPACK_IMPORTED_MODULE_0__["TestBed"].createComponent(_search_images_component__WEBPACK_IMPORTED_MODULE_1__["SearchImagesComponent"]);
        component = fixture.componentInstance;
        fixture.detectChanges();
    });
    // verifica que se pueda crear los componentes. 
    it('Conponente creado y validado', () => {
        expect(component).toBeTruthy();
    });
});


/***/ }),

/***/ "./src/app/search-images/search-images.component.ts":
/*!**********************************************************!*\
  !*** ./src/app/search-images/search-images.component.ts ***!
  \**********************************************************/
/*! exports provided: SearchImagesComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SearchImagesComponent", function() { return SearchImagesComponent; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _services_flickr_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../services/flickr.service */ "./src/app/services/flickr.service.ts");



let SearchImagesComponent = class SearchImagesComponent {
    constructor(flickrService) {
        this.flickrService = flickrService;
        this.imagenes = [];
        this.favorites = [];
        this.keyword = "Aguascalientes";
        // to show up Aguascalientes
        this.flickrService.search_keyword(this.keyword)
            .toPromise()
            .then(res => {
            this.imagenes = res;
        });
    }
    ngOnInit() {
    }
    search(event) {
        this.keyword = event.target.value.toLowerCase();
        if (this.keyword && this.keyword.length > 0) {
            this.flickrService.search_keyword(this.keyword)
                .toPromise()
                .then(res => {
                this.imagenes = res;
            });
        }
    }
    onScroll() {
        if (this.keyword && this.keyword.length > 0) {
            this.flickrService.search_keyword(this.keyword)
                .toPromise()
                .then(res => {
                this.imagenes = this.imagenes.concat(res);
            });
        }
    }
    onSelect(imagen) {
        imagen.favorito = !imagen.favorito;
        //to keep in memory the current state
        localStorage.setItem('favoritos' + imagen.id, imagen.favorito);
    }
    readLocalStorageValue(key) {
        return localStorage.getItem(key);
    }
};
SearchImagesComponent.ctorParameters = () => [
    { type: _services_flickr_service__WEBPACK_IMPORTED_MODULE_2__["FlickrService"] }
];
SearchImagesComponent = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Component"])({
        selector: 'app-search-images',
        template: Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! raw-loader!./search-images.component.html */ "./node_modules/raw-loader/dist/cjs.js!./src/app/search-images/search-images.component.html")).default,
        styles: [Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__importDefault"])(__webpack_require__(/*! ./search-images.component.css */ "./src/app/search-images/search-images.component.css")).default]
    })
], SearchImagesComponent);



/***/ }),

/***/ "./src/app/services/flickr.service.spec.ts":
/*!*************************************************!*\
  !*** ./src/app/services/flickr.service.spec.ts ***!
  \*************************************************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core_testing__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core/testing */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/testing.js");
/* harmony import */ var _flickr_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./flickr.service */ "./src/app/services/flickr.service.ts");
/* harmony import */ var _angular_common_http_testing__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http/testing */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http/testing.js");



describe('FlickrService', () => {
    beforeEach(() => _angular_core_testing__WEBPACK_IMPORTED_MODULE_0__["TestBed"].configureTestingModule({
        imports: [_angular_common_http_testing__WEBPACK_IMPORTED_MODULE_2__["HttpClientTestingModule"]]
    }));
    //probando conexion a flicker y paramametros que puedan obtener datos
    it('Creacion exitosa y verifica conexion http al webservice', () => {
        const service = _angular_core_testing__WEBPACK_IMPORTED_MODULE_0__["TestBed"].get(_flickr_service__WEBPACK_IMPORTED_MODULE_1__["FlickrService"]);
        expect(service).toBeTruthy();
    });
});


/***/ }),

/***/ "./src/app/services/flickr.service.ts":
/*!********************************************!*\
  !*** ./src/app/services/flickr.service.ts ***!
  \********************************************/
/*! exports provided: FlickrService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "FlickrService", function() { return FlickrService; });
/* harmony import */ var tslib__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! tslib */ "./node_modules/tslib/tslib.es6.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/__ivy_ngcc__/fesm2015/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm2015/operators/index.js");
/* harmony import */ var src_environments_environment__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! src/environments/environment */ "./src/environments/environment.ts");





let FlickrService = class FlickrService {
    constructor(http) {
        this.http = http;
        this.currPage = 1;
    }
    search_keyword(keyword) {
        if (this.prevKeyword === keyword) {
            this.currPage++;
        }
        else {
            this.currPage = 1;
        }
        this.prevKeyword = keyword;
        const url = 'https://www.flickr.com/services/rest/?method=flickr.photos.search&';
        const params = `api_key=${src_environments_environment__WEBPACK_IMPORTED_MODULE_4__["environment"].flickr.key}&text=${keyword}&format=json&nojsoncallback=1&per_page=12&page=${this.currPage}`;
        return this.http.get(url + params).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_3__["map"])((res) => {
            const urlArr = [];
            console.log(url + params);
            res.photos.photo.forEach((ph) => {
                // Recovery the last stage in memory  from favoritos
                let favoritoVar = localStorage.getItem('favoritos' + ph.id) ? localStorage.getItem('favoritos' + ph.id) : false;
                //console.log (favoritoVar); manual testing done
                const photoObj = {
                    id: ph.id,
                    url: `https://farm${ph.farm}.staticflickr.com/${ph.server}/${ph.id}_${ph.secret}`,
                    title: ph.title,
                    favorito: favoritoVar
                };
                urlArr.push(photoObj);
            });
            return urlArr;
        }));
    }
};
FlickrService.ctorParameters = () => [
    { type: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClient"] }
];
FlickrService = Object(tslib__WEBPACK_IMPORTED_MODULE_0__["__decorate"])([
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
        providedIn: 'root'
    })
], FlickrService);



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build --prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
const environment = {
    production: false,
    flickr: {
        key: '5a85aa8d369505d6befa57985ddfa686'
    }
};
/*
 * For easier debugging in development mode, you can import the following file
 * to ignore zone related error stack frames such as `zone.run`, `zoneDelegate.invokeTask`.
 *
 * This import should be commented out in production mode because it will have a negative impact
 * on performance if an error is thrown.
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/test.ts":
/*!*********************!*\
  !*** ./src/test.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var zone_js_dist_zone_testing__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! zone.js/dist/zone-testing */ "./node_modules/zone.js/dist/zone-testing.js");
/* harmony import */ var zone_js_dist_zone_testing__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(zone_js_dist_zone_testing__WEBPACK_IMPORTED_MODULE_0__);
/* harmony import */ var _angular_core_testing__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core/testing */ "./node_modules/@angular/core/__ivy_ngcc__/fesm2015/testing.js");
/* harmony import */ var _angular_platform_browser_dynamic_testing__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/platform-browser-dynamic/testing */ "./node_modules/@angular/platform-browser-dynamic/__ivy_ngcc__/fesm2015/testing.js");
// This file is required by karma.conf.js and loads recursively all the .spec and framework files



// First, initialize the Angular testing environment.
Object(_angular_core_testing__WEBPACK_IMPORTED_MODULE_1__["getTestBed"])().initTestEnvironment(_angular_platform_browser_dynamic_testing__WEBPACK_IMPORTED_MODULE_2__["BrowserDynamicTestingModule"], Object(_angular_platform_browser_dynamic_testing__WEBPACK_IMPORTED_MODULE_2__["platformBrowserDynamicTesting"])());
// Then we find all the tests.
const context = __webpack_require__("./src sync recursive \\.spec\\.ts$");
// And load the modules.
context.keys().map(context);


/***/ })

/******/ });
//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJzb3VyY2VzIjpbIndlYnBhY2svYm9vdHN0cmFwIiwiLi8kX2xhenlfcm91dGVfcmVzb3VyY2UgbGF6eSBuYW1lc3BhY2Ugb2JqZWN0IiwiLi9zcmMvYXBwL2FwcC5jb21wb25lbnQuaHRtbCIsIi4vc3JjL2FwcC9zZWFyY2gtaW1hZ2VzL3NlYXJjaC1pbWFnZXMuY29tcG9uZW50Lmh0bWwiLCIod2VicGFjaykvYnVpbGRpbi9nbG9iYWwuanMiLCIuL3NyYyBzeW5jIFxcLnNwZWNcXC50cyQiLCIuL3NyYy9hcHAvYXBwLmNvbXBvbmVudC5jc3MiLCIuL3NyYy9hcHAvYXBwLmNvbXBvbmVudC5zcGVjLnRzIiwiLi9zcmMvYXBwL2FwcC5jb21wb25lbnQudHMiLCIuL3NyYy9hcHAvc2VhcmNoLWltYWdlcy9zZWFyY2gtaW1hZ2VzLmNvbXBvbmVudC5jc3MiLCIuL3NyYy9hcHAvc2VhcmNoLWltYWdlcy9zZWFyY2gtaW1hZ2VzLmNvbXBvbmVudC5zcGVjLnRzIiwiLi9zcmMvYXBwL3NlYXJjaC1pbWFnZXMvc2VhcmNoLWltYWdlcy5jb21wb25lbnQudHMiLCIuL3NyYy9hcHAvc2VydmljZXMvZmxpY2tyLnNlcnZpY2Uuc3BlYy50cyIsIi4vc3JjL2FwcC9zZXJ2aWNlcy9mbGlja3Iuc2VydmljZS50cyIsIi4vc3JjL2Vudmlyb25tZW50cy9lbnZpcm9ubWVudC50cyIsIi4vc3JjL3Rlc3QudHMiXSwibmFtZXMiOltdLCJtYXBwaW5ncyI6IjtRQUFBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0EsUUFBUSxvQkFBb0I7UUFDNUI7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQSxpQkFBaUIsNEJBQTRCO1FBQzdDO1FBQ0E7UUFDQSxrQkFBa0IsMkJBQTJCO1FBQzdDO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTs7UUFFQTs7UUFFQTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBOztRQUVBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7OztRQUdBO1FBQ0E7O1FBRUE7UUFDQTs7UUFFQTtRQUNBO1FBQ0E7UUFDQSwwQ0FBMEMsZ0NBQWdDO1FBQzFFO1FBQ0E7O1FBRUE7UUFDQTtRQUNBO1FBQ0Esd0RBQXdELGtCQUFrQjtRQUMxRTtRQUNBLGlEQUFpRCxjQUFjO1FBQy9EOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQTtRQUNBO1FBQ0E7UUFDQSx5Q0FBeUMsaUNBQWlDO1FBQzFFLGdIQUFnSCxtQkFBbUIsRUFBRTtRQUNySTtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBLDJCQUEyQiwwQkFBMEIsRUFBRTtRQUN2RCxpQ0FBaUMsZUFBZTtRQUNoRDtRQUNBO1FBQ0E7O1FBRUE7UUFDQSxzREFBc0QsK0RBQStEOztRQUVySDtRQUNBOztRQUVBO1FBQ0E7UUFDQTtRQUNBO1FBQ0EsZ0JBQWdCLHVCQUF1QjtRQUN2Qzs7O1FBR0E7UUFDQTtRQUNBO1FBQ0E7Ozs7Ozs7Ozs7OztBQ3ZKQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLEVBQUU7QUFDRjtBQUNBLDRDQUE0QyxXQUFXO0FBQ3ZEO0FBQ0E7QUFDQSx3RTs7Ozs7Ozs7Ozs7O0FDWkE7QUFBZSwwR0FBMkMsRTs7Ozs7Ozs7Ozs7O0FDQTFEO0FBQWUsc0dBQXVDLDJVQUEyVSxZQUFZLG1CQUFtQixjQUFjLHNXQUFzVyxFOzs7Ozs7Ozs7OztBQ0FweEI7O0FBRUE7QUFDQTtBQUNBO0FBQ0EsQ0FBQzs7QUFFRDtBQUNBO0FBQ0E7QUFDQSxDQUFDO0FBQ0Q7QUFDQTtBQUNBOztBQUVBO0FBQ0E7QUFDQSw0Q0FBNEM7O0FBRTVDOzs7Ozs7Ozs7Ozs7QUNuQkE7QUFDQTtBQUNBO0FBQ0E7QUFDQTs7O0FBR0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBO0FBQ0E7QUFDQTtBQUNBLHlEOzs7Ozs7Ozs7Ozs7QUN4QkE7QUFBZSw2R0FBOEMsK0hBQStILEU7Ozs7Ozs7Ozs7OztBQ0E1TDtBQUFBO0FBQUE7QUFBdUQ7QUFDUjtBQUUvQyxRQUFRLENBQUMsY0FBYyxFQUFFLEdBQUcsRUFBRTtJQUM1QixVQUFVLENBQUMsbUVBQUssQ0FBQyxHQUFHLEVBQUU7UUFDcEIsNkRBQU8sQ0FBQyxzQkFBc0IsQ0FBQztZQUM3QixZQUFZLEVBQUU7Z0JBQ1osMkRBQVk7YUFDYjtTQUNGLENBQUMsQ0FBQyxpQkFBaUIsRUFBRSxDQUFDO0lBQ3pCLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFFSixFQUFFLENBQUMseUJBQXlCLEVBQUUsR0FBRyxFQUFFO1FBQ2pDLE1BQU0sT0FBTyxHQUFHLDZEQUFPLENBQUMsZUFBZSxDQUFDLDJEQUFZLENBQUMsQ0FBQztRQUN0RCxNQUFNLEdBQUcsR0FBRyxPQUFPLENBQUMsWUFBWSxDQUFDLGlCQUFpQixDQUFDO1FBQ25ELE1BQU0sQ0FBQyxHQUFHLENBQUMsQ0FBQyxVQUFVLEVBQUUsQ0FBQztJQUMzQixDQUFDLENBQUMsQ0FBQztJQUVILEVBQUUsQ0FBQyxzQ0FBc0MsRUFBRSxHQUFHLEVBQUU7UUFDOUMsaURBQWlEO1FBQ2pELE1BQU0sT0FBTyxHQUFHLDZEQUFPLENBQUMsZUFBZSxDQUFDLDJEQUFZLENBQUMsQ0FBQztRQUN0RCxNQUFNLEdBQUcsR0FBRyxPQUFPLENBQUMsWUFBWSxDQUFDLGlCQUFpQixDQUFDO1FBQ25ELE1BQU0sQ0FBQyxHQUFHLENBQUMsS0FBSyxDQUFDLENBQUMsT0FBTyxDQUFDLGlCQUFpQixDQUFDLENBQUM7SUFFL0MsQ0FBQyxDQUFDLENBQUM7QUFHTCxDQUFDLENBQUMsQ0FBQzs7Ozs7Ozs7Ozs7Ozs7Ozs7O0FDM0J1QztBQU8xQyxJQUFhLFlBQVksR0FBekIsTUFBYSxZQUFZO0lBQXpCO1FBQ0UscUNBQXFDO1FBQ3JDLFVBQUssR0FBRyxpQkFBaUIsQ0FBQztJQUM1QixDQUFDO0NBQUE7QUFIWSxZQUFZO0lBTHhCLCtEQUFTLENBQUM7UUFDVCxRQUFRLEVBQUUsVUFBVTtRQUNwQixpTkFBbUM7O0tBRXBDLENBQUM7R0FDVyxZQUFZLENBR3hCO0FBSHdCOzs7Ozs7Ozs7Ozs7O0FDUHpCO0FBQWUsa0VBQUcsMkJBQTJCLEdBQUcsWUFBWSx1QkFBdUIsMEJBQTBCLDBEQUEwRCxlQUFlLHFCQUFxQixHQUFHLGlCQUFpQix3QkFBd0IsZ0NBQWdDLE1BQU0sa0JBQWtCLHVCQUF1QixpQkFBaUIsbUJBQW1CLFlBQVksYUFBYSwrQkFBK0IsYUFBYSxrR0FBa0csa0NBQWtDLGdCQUFnQixlQUFlLDZDQUE2QyxnQkFBZ0Isa0JBQWtCLG1CQUFtQixnQkFBZ0IsR0FBRyxXQUFXLHFCQUFxQixzQkFBc0IsOEJBQThCLGtCQUFrQixpQkFBaUIsdUJBQXVCLG9CQUFvQiw2RUFBNkUsR0FBRyxTQUFTLHlCQUF5Qix5QkFBeUIsZ0JBQWdCLGtCQUFrQixHQUFHLDBDQUEwQyxhQUFhLGlCQUFpQixLQUFLLEdBQUcsMENBQTBDLGFBQWEsa0JBQWtCLEtBQUssR0FBRywrQ0FBK0MsbWdGQUFtZ0YsRTs7Ozs7Ozs7Ozs7O0FDQTV1SDtBQUFBO0FBQUE7QUFBQTtBQUF5RTtBQUVQO0FBQ0k7QUFFdEUsUUFBUSxDQUFDLHVCQUF1QixFQUFFLEdBQUcsRUFBRTtJQUNyQyxJQUFJLFNBQWdDLENBQUM7SUFDckMsSUFBSSxPQUFnRCxDQUFDO0lBRXJELFVBQVUsQ0FBQyxtRUFBSyxDQUFDLEdBQUcsRUFBRTtRQUNwQiw2REFBTyxDQUFDLHNCQUFzQixDQUFDO1lBQzdCLE9BQU8sRUFBRSxDQUFDLG9GQUF1QixDQUFDO1lBRWxDLFlBQVksRUFBRSxDQUFFLDhFQUFxQixDQUFFO1NBQ3hDLENBQUM7YUFDRCxpQkFBaUIsRUFBRSxDQUFDO0lBQ3ZCLENBQUMsQ0FBQyxDQUFDLENBQUM7SUFFSixVQUFVLENBQUMsR0FBRyxFQUFFO1FBQ2QsT0FBTyxHQUFHLDZEQUFPLENBQUMsZUFBZSxDQUFDLDhFQUFxQixDQUFDLENBQUM7UUFDekQsU0FBUyxHQUFHLE9BQU8sQ0FBQyxpQkFBaUIsQ0FBQztRQUN0QyxPQUFPLENBQUMsYUFBYSxFQUFFLENBQUM7SUFDMUIsQ0FBQyxDQUFDLENBQUM7SUFFTCxnREFBZ0Q7SUFDOUMsRUFBRSxDQUFDLDhCQUE4QixFQUFFLEdBQUcsRUFBRTtRQUN0QyxNQUFNLENBQUMsU0FBUyxDQUFDLENBQUMsVUFBVSxFQUFFLENBQUM7SUFDakMsQ0FBQyxDQUFDLENBQUM7QUFDTCxDQUFDLENBQUMsQ0FBQzs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQzVCK0M7QUFDUztBQU8zRCxJQUFhLHFCQUFxQixHQUFsQyxNQUFhLHFCQUFxQjtJQUtoQyxZQUFvQixhQUE0QjtRQUE1QixrQkFBYSxHQUFiLGFBQWEsQ0FBZTtRQUpoRCxhQUFRLEdBQUcsRUFBRSxDQUFDO1FBQ2QsY0FBUyxHQUFHLEVBQUUsQ0FBQztRQUNmLFlBQU8sR0FBVSxnQkFBZ0IsQ0FBQztRQUdoQyw0QkFBNEI7UUFDNUIsSUFBSSxDQUFDLGFBQWEsQ0FBQyxjQUFjLENBQUMsSUFBSSxDQUFDLE9BQU8sQ0FBQzthQUM5QyxTQUFTLEVBQUU7YUFDWCxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUU7WUFDVixJQUFJLENBQUMsUUFBUSxHQUFHLEdBQUcsQ0FBQztRQUN0QixDQUFDLENBQUMsQ0FBQztJQUNMLENBQUM7SUFFRCxRQUFRO0lBRVIsQ0FBQztJQUVELE1BQU0sQ0FBQyxLQUFVO1FBQ2YsSUFBSSxDQUFDLE9BQU8sR0FBRyxLQUFLLENBQUMsTUFBTSxDQUFDLEtBQUssQ0FBQyxXQUFXLEVBQUUsQ0FBQztRQUNoRCxJQUFJLElBQUksQ0FBQyxPQUFPLElBQUksSUFBSSxDQUFDLE9BQU8sQ0FBQyxNQUFNLEdBQUcsQ0FBQyxFQUFFO1lBQzNDLElBQUksQ0FBQyxhQUFhLENBQUMsY0FBYyxDQUFDLElBQUksQ0FBQyxPQUFPLENBQUM7aUJBQzVDLFNBQVMsRUFBRTtpQkFDWCxJQUFJLENBQUMsR0FBRyxDQUFDLEVBQUU7Z0JBQ1YsSUFBSSxDQUFDLFFBQVEsR0FBRyxHQUFHLENBQUM7WUFDdEIsQ0FBQyxDQUFDLENBQUM7U0FDTjtJQUNILENBQUM7SUFFRCxRQUFRO1FBQ04sSUFBSSxJQUFJLENBQUMsT0FBTyxJQUFJLElBQUksQ0FBQyxPQUFPLENBQUMsTUFBTSxHQUFHLENBQUMsRUFBRTtZQUMzQyxJQUFJLENBQUMsYUFBYSxDQUFDLGNBQWMsQ0FBQyxJQUFJLENBQUMsT0FBTyxDQUFDO2lCQUM1QyxTQUFTLEVBQUU7aUJBQ1gsSUFBSSxDQUFDLEdBQUcsQ0FBQyxFQUFFO2dCQUNWLElBQUksQ0FBQyxRQUFRLEdBQUcsSUFBSSxDQUFDLFFBQVEsQ0FBQyxNQUFNLENBQUMsR0FBRyxDQUFDLENBQUM7WUFDNUMsQ0FBQyxDQUFDLENBQUM7U0FDTjtJQUVILENBQUM7SUFFRCxRQUFRLENBQUMsTUFBVztRQUNsQixNQUFNLENBQUMsUUFBUSxHQUFHLENBQUMsTUFBTSxDQUFDLFFBQVEsQ0FBQztRQUVuQyxxQ0FBcUM7UUFDckMsWUFBWSxDQUFDLE9BQU8sQ0FBQyxXQUFXLEdBQUMsTUFBTSxDQUFDLEVBQUUsRUFBQyxNQUFNLENBQUMsUUFBUSxDQUFDLENBQUM7SUFDOUQsQ0FBQztJQUVELHFCQUFxQixDQUFDLEdBQUc7UUFDdkIsT0FBTyxZQUFZLENBQUMsT0FBTyxDQUFDLEdBQUcsQ0FBQyxDQUFDO0lBQ25DLENBQUM7Q0FFRjs7WUE5Q29DLHNFQUFhOztBQUxyQyxxQkFBcUI7SUFMakMsK0RBQVMsQ0FBQztRQUNULFFBQVEsRUFBRSxtQkFBbUI7UUFDN0IsbVBBQTZDOztLQUU5QyxDQUFDO0dBQ1cscUJBQXFCLENBbURqQztBQW5EaUM7Ozs7Ozs7Ozs7Ozs7QUNSbEM7QUFBQTtBQUFBO0FBQUE7QUFBZ0Q7QUFFQztBQUNxQjtBQUV0RSxRQUFRLENBQUMsZUFBZSxFQUFFLEdBQUcsRUFBRTtJQUM3QixVQUFVLENBQUMsR0FBRyxFQUFFLENBQUMsNkRBQU8sQ0FBQyxzQkFBc0IsQ0FBQztRQUM5QyxPQUFPLEVBQUUsQ0FBQyxvRkFBdUIsQ0FBQztLQUVuQyxDQUFDLENBQUMsQ0FBQztJQUVKLHFFQUFxRTtJQUNyRSxFQUFFLENBQUMseURBQXlELEVBQUUsR0FBRyxFQUFFO1FBQ2pFLE1BQU0sT0FBTyxHQUFrQiw2REFBTyxDQUFDLEdBQUcsQ0FBQyw2REFBYSxDQUFDLENBQUM7UUFDMUQsTUFBTSxDQUFDLE9BQU8sQ0FBQyxDQUFDLFVBQVUsRUFBRSxDQUFDO0lBQy9CLENBQUMsQ0FBQyxDQUFDO0FBQ0wsQ0FBQyxDQUFDLENBQUM7Ozs7Ozs7Ozs7Ozs7Ozs7Ozs7OztBQ2hCZ0U7QUFDakI7QUFDYjtBQUNzQjtBQW1CM0QsSUFBYSxhQUFhLEdBQTFCLE1BQWEsYUFBYTtJQUl4QixZQUFvQixJQUFnQjtRQUFoQixTQUFJLEdBQUosSUFBSSxDQUFZO1FBRnBDLGFBQVEsR0FBRyxDQUFDLENBQUM7SUFFMkIsQ0FBQztJQUV6QyxjQUFjLENBQUMsT0FBZTtRQUM1QixJQUFJLElBQUksQ0FBQyxXQUFXLEtBQUssT0FBTyxFQUFFO1lBQ2hDLElBQUksQ0FBQyxRQUFRLEVBQUUsQ0FBQztTQUNqQjthQUFNO1lBQ0wsSUFBSSxDQUFDLFFBQVEsR0FBRyxDQUFDLENBQUM7U0FDbkI7UUFDRCxJQUFJLENBQUMsV0FBVyxHQUFHLE9BQU8sQ0FBQztRQUMzQixNQUFNLEdBQUcsR0FBRyxvRUFBb0UsQ0FBQztRQUNqRixNQUFNLE1BQU0sR0FBRyxXQUFXLHdFQUFXLENBQUMsTUFBTSxDQUFDLEdBQUcsU0FBUyxPQUFPLGtEQUFrRCxJQUFJLENBQUMsUUFBUSxFQUFFLENBQUM7UUFFbEksT0FBTyxJQUFJLENBQUMsSUFBSSxDQUFDLEdBQUcsQ0FBQyxHQUFHLEdBQUcsTUFBTSxDQUFDLENBQUMsSUFBSSxDQUFDLDBEQUFHLENBQUMsQ0FBQyxHQUFpQixFQUFFLEVBQUU7WUFDaEUsTUFBTSxNQUFNLEdBQUcsRUFBRSxDQUFDO1lBQ2xCLE9BQU8sQ0FBQyxHQUFHLENBQUMsR0FBRyxHQUFDLE1BQU0sQ0FBQyxDQUFDO1lBQ3hCLEdBQUcsQ0FBQyxNQUFNLENBQUMsS0FBSyxDQUFDLE9BQU8sQ0FBQyxDQUFDLEVBQWUsRUFBRSxFQUFFO2dCQUNuRCxvREFBb0Q7Z0JBQzlDLElBQUksV0FBVyxHQUFHLFlBQVksQ0FBQyxPQUFPLENBQUMsV0FBVyxHQUFDLEVBQUUsQ0FBQyxFQUFFLENBQUMsRUFBQyxhQUFZLENBQUMsT0FBTyxDQUFDLFdBQVcsR0FBQyxFQUFFLENBQUMsRUFBRSxDQUFDLEVBQUMsTUFBSyxDQUFDO2dCQUM5RyxnREFBZ0Q7Z0JBQ3hDLE1BQU0sUUFBUSxHQUFHO29CQUNmLEVBQUUsRUFBRyxFQUFFLENBQUMsRUFBRTtvQkFDVixHQUFHLEVBQUUsZUFBZSxFQUFFLENBQUMsSUFBSSxxQkFBcUIsRUFBRSxDQUFDLE1BQU0sSUFBSSxFQUFFLENBQUMsRUFBRSxJQUFJLEVBQUUsQ0FBQyxNQUFNLEVBQUU7b0JBQ2pGLEtBQUssRUFBRSxFQUFFLENBQUMsS0FBSztvQkFDZixRQUFRLEVBQUMsV0FBVztpQkFDckIsQ0FBQztnQkFDRixNQUFNLENBQUMsSUFBSSxDQUFDLFFBQVEsQ0FBQyxDQUFDO1lBQ3hCLENBQUMsQ0FBQyxDQUFDO1lBQ0gsT0FBTyxNQUFNLENBQUM7UUFDaEIsQ0FBQyxDQUFDLENBQUMsQ0FBQztJQUNOLENBQUM7Q0FDRjs7WUE5QjJCLCtEQUFVOztBQUp6QixhQUFhO0lBSHpCLGdFQUFVLENBQUM7UUFDVixVQUFVLEVBQUUsTUFBTTtLQUNuQixDQUFDO0dBQ1csYUFBYSxDQWtDekI7QUFsQ3lCOzs7Ozs7Ozs7Ozs7O0FDdEIxQjtBQUFBO0FBQUEsZ0ZBQWdGO0FBQ2hGLDBFQUEwRTtBQUMxRSxnRUFBZ0U7QUFFekQsTUFBTSxXQUFXLEdBQUc7SUFDekIsVUFBVSxFQUFFLEtBQUs7SUFDakIsTUFBTSxFQUFFO1FBQ04sR0FBRyxFQUFFLGtDQUFrQztLQUN4QztDQUNGLENBQUM7QUFFRjs7Ozs7O0dBTUc7QUFDSCxtRUFBbUU7Ozs7Ozs7Ozs7Ozs7QUNsQm5FO0FBQUE7QUFBQTtBQUFBO0FBQUE7QUFBQSxpR0FBaUc7QUFFOUQ7QUFDZ0I7QUFJQTtBQUluRCxxREFBcUQ7QUFDckQsd0VBQVUsRUFBRSxDQUFDLG1CQUFtQixDQUM5QixxR0FBMkIsRUFDM0IsK0dBQTZCLEVBQUUsQ0FDaEMsQ0FBQztBQUNGLDhCQUE4QjtBQUM5QixNQUFNLE9BQU8sR0FBRyx5REFBMEMsQ0FBQztBQUMzRCx3QkFBd0I7QUFDeEIsT0FBTyxDQUFDLElBQUksRUFBRSxDQUFDLEdBQUcsQ0FBQyxPQUFPLENBQUMsQ0FBQyIsImZpbGUiOiJtYWluLmpzIiwic291cmNlc0NvbnRlbnQiOlsiIFx0Ly8gaW5zdGFsbCBhIEpTT05QIGNhbGxiYWNrIGZvciBjaHVuayBsb2FkaW5nXG4gXHRmdW5jdGlvbiB3ZWJwYWNrSnNvbnBDYWxsYmFjayhkYXRhKSB7XG4gXHRcdHZhciBjaHVua0lkcyA9IGRhdGFbMF07XG4gXHRcdHZhciBtb3JlTW9kdWxlcyA9IGRhdGFbMV07XG4gXHRcdHZhciBleGVjdXRlTW9kdWxlcyA9IGRhdGFbMl07XG5cbiBcdFx0Ly8gYWRkIFwibW9yZU1vZHVsZXNcIiB0byB0aGUgbW9kdWxlcyBvYmplY3QsXG4gXHRcdC8vIHRoZW4gZmxhZyBhbGwgXCJjaHVua0lkc1wiIGFzIGxvYWRlZCBhbmQgZmlyZSBjYWxsYmFja1xuIFx0XHR2YXIgbW9kdWxlSWQsIGNodW5rSWQsIGkgPSAwLCByZXNvbHZlcyA9IFtdO1xuIFx0XHRmb3IoO2kgPCBjaHVua0lkcy5sZW5ndGg7IGkrKykge1xuIFx0XHRcdGNodW5rSWQgPSBjaHVua0lkc1tpXTtcbiBcdFx0XHRpZihPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwoaW5zdGFsbGVkQ2h1bmtzLCBjaHVua0lkKSAmJiBpbnN0YWxsZWRDaHVua3NbY2h1bmtJZF0pIHtcbiBcdFx0XHRcdHJlc29sdmVzLnB1c2goaW5zdGFsbGVkQ2h1bmtzW2NodW5rSWRdWzBdKTtcbiBcdFx0XHR9XG4gXHRcdFx0aW5zdGFsbGVkQ2h1bmtzW2NodW5rSWRdID0gMDtcbiBcdFx0fVxuIFx0XHRmb3IobW9kdWxlSWQgaW4gbW9yZU1vZHVsZXMpIHtcbiBcdFx0XHRpZihPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwobW9yZU1vZHVsZXMsIG1vZHVsZUlkKSkge1xuIFx0XHRcdFx0bW9kdWxlc1ttb2R1bGVJZF0gPSBtb3JlTW9kdWxlc1ttb2R1bGVJZF07XG4gXHRcdFx0fVxuIFx0XHR9XG4gXHRcdGlmKHBhcmVudEpzb25wRnVuY3Rpb24pIHBhcmVudEpzb25wRnVuY3Rpb24oZGF0YSk7XG5cbiBcdFx0d2hpbGUocmVzb2x2ZXMubGVuZ3RoKSB7XG4gXHRcdFx0cmVzb2x2ZXMuc2hpZnQoKSgpO1xuIFx0XHR9XG5cbiBcdFx0Ly8gYWRkIGVudHJ5IG1vZHVsZXMgZnJvbSBsb2FkZWQgY2h1bmsgdG8gZGVmZXJyZWQgbGlzdFxuIFx0XHRkZWZlcnJlZE1vZHVsZXMucHVzaC5hcHBseShkZWZlcnJlZE1vZHVsZXMsIGV4ZWN1dGVNb2R1bGVzIHx8IFtdKTtcblxuIFx0XHQvLyBydW4gZGVmZXJyZWQgbW9kdWxlcyB3aGVuIGFsbCBjaHVua3MgcmVhZHlcbiBcdFx0cmV0dXJuIGNoZWNrRGVmZXJyZWRNb2R1bGVzKCk7XG4gXHR9O1xuIFx0ZnVuY3Rpb24gY2hlY2tEZWZlcnJlZE1vZHVsZXMoKSB7XG4gXHRcdHZhciByZXN1bHQ7XG4gXHRcdGZvcih2YXIgaSA9IDA7IGkgPCBkZWZlcnJlZE1vZHVsZXMubGVuZ3RoOyBpKyspIHtcbiBcdFx0XHR2YXIgZGVmZXJyZWRNb2R1bGUgPSBkZWZlcnJlZE1vZHVsZXNbaV07XG4gXHRcdFx0dmFyIGZ1bGZpbGxlZCA9IHRydWU7XG4gXHRcdFx0Zm9yKHZhciBqID0gMTsgaiA8IGRlZmVycmVkTW9kdWxlLmxlbmd0aDsgaisrKSB7XG4gXHRcdFx0XHR2YXIgZGVwSWQgPSBkZWZlcnJlZE1vZHVsZVtqXTtcbiBcdFx0XHRcdGlmKGluc3RhbGxlZENodW5rc1tkZXBJZF0gIT09IDApIGZ1bGZpbGxlZCA9IGZhbHNlO1xuIFx0XHRcdH1cbiBcdFx0XHRpZihmdWxmaWxsZWQpIHtcbiBcdFx0XHRcdGRlZmVycmVkTW9kdWxlcy5zcGxpY2UoaS0tLCAxKTtcbiBcdFx0XHRcdHJlc3VsdCA9IF9fd2VicGFja19yZXF1aXJlX18oX193ZWJwYWNrX3JlcXVpcmVfXy5zID0gZGVmZXJyZWRNb2R1bGVbMF0pO1xuIFx0XHRcdH1cbiBcdFx0fVxuXG4gXHRcdHJldHVybiByZXN1bHQ7XG4gXHR9XG5cbiBcdC8vIFRoZSBtb2R1bGUgY2FjaGVcbiBcdHZhciBpbnN0YWxsZWRNb2R1bGVzID0ge307XG5cbiBcdC8vIG9iamVjdCB0byBzdG9yZSBsb2FkZWQgYW5kIGxvYWRpbmcgY2h1bmtzXG4gXHQvLyB1bmRlZmluZWQgPSBjaHVuayBub3QgbG9hZGVkLCBudWxsID0gY2h1bmsgcHJlbG9hZGVkL3ByZWZldGNoZWRcbiBcdC8vIFByb21pc2UgPSBjaHVuayBsb2FkaW5nLCAwID0gY2h1bmsgbG9hZGVkXG4gXHR2YXIgaW5zdGFsbGVkQ2h1bmtzID0ge1xuIFx0XHRcIm1haW5cIjogMFxuIFx0fTtcblxuIFx0dmFyIGRlZmVycmVkTW9kdWxlcyA9IFtdO1xuXG4gXHQvLyBUaGUgcmVxdWlyZSBmdW5jdGlvblxuIFx0ZnVuY3Rpb24gX193ZWJwYWNrX3JlcXVpcmVfXyhtb2R1bGVJZCkge1xuXG4gXHRcdC8vIENoZWNrIGlmIG1vZHVsZSBpcyBpbiBjYWNoZVxuIFx0XHRpZihpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSkge1xuIFx0XHRcdHJldHVybiBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXS5leHBvcnRzO1xuIFx0XHR9XG4gXHRcdC8vIENyZWF0ZSBhIG5ldyBtb2R1bGUgKGFuZCBwdXQgaXQgaW50byB0aGUgY2FjaGUpXG4gXHRcdHZhciBtb2R1bGUgPSBpbnN0YWxsZWRNb2R1bGVzW21vZHVsZUlkXSA9IHtcbiBcdFx0XHRpOiBtb2R1bGVJZCxcbiBcdFx0XHRsOiBmYWxzZSxcbiBcdFx0XHRleHBvcnRzOiB7fVxuIFx0XHR9O1xuXG4gXHRcdC8vIEV4ZWN1dGUgdGhlIG1vZHVsZSBmdW5jdGlvblxuIFx0XHRtb2R1bGVzW21vZHVsZUlkXS5jYWxsKG1vZHVsZS5leHBvcnRzLCBtb2R1bGUsIG1vZHVsZS5leHBvcnRzLCBfX3dlYnBhY2tfcmVxdWlyZV9fKTtcblxuIFx0XHQvLyBGbGFnIHRoZSBtb2R1bGUgYXMgbG9hZGVkXG4gXHRcdG1vZHVsZS5sID0gdHJ1ZTtcblxuIFx0XHQvLyBSZXR1cm4gdGhlIGV4cG9ydHMgb2YgdGhlIG1vZHVsZVxuIFx0XHRyZXR1cm4gbW9kdWxlLmV4cG9ydHM7XG4gXHR9XG5cblxuIFx0Ly8gZXhwb3NlIHRoZSBtb2R1bGVzIG9iamVjdCAoX193ZWJwYWNrX21vZHVsZXNfXylcbiBcdF9fd2VicGFja19yZXF1aXJlX18ubSA9IG1vZHVsZXM7XG5cbiBcdC8vIGV4cG9zZSB0aGUgbW9kdWxlIGNhY2hlXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLmMgPSBpbnN0YWxsZWRNb2R1bGVzO1xuXG4gXHQvLyBkZWZpbmUgZ2V0dGVyIGZ1bmN0aW9uIGZvciBoYXJtb255IGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uZCA9IGZ1bmN0aW9uKGV4cG9ydHMsIG5hbWUsIGdldHRlcikge1xuIFx0XHRpZighX193ZWJwYWNrX3JlcXVpcmVfXy5vKGV4cG9ydHMsIG5hbWUpKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIG5hbWUsIHsgZW51bWVyYWJsZTogdHJ1ZSwgZ2V0OiBnZXR0ZXIgfSk7XG4gXHRcdH1cbiBcdH07XG5cbiBcdC8vIGRlZmluZSBfX2VzTW9kdWxlIG9uIGV4cG9ydHNcbiBcdF9fd2VicGFja19yZXF1aXJlX18uciA9IGZ1bmN0aW9uKGV4cG9ydHMpIHtcbiBcdFx0aWYodHlwZW9mIFN5bWJvbCAhPT0gJ3VuZGVmaW5lZCcgJiYgU3ltYm9sLnRvU3RyaW5nVGFnKSB7XG4gXHRcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsIFN5bWJvbC50b1N0cmluZ1RhZywgeyB2YWx1ZTogJ01vZHVsZScgfSk7XG4gXHRcdH1cbiBcdFx0T2JqZWN0LmRlZmluZVByb3BlcnR5KGV4cG9ydHMsICdfX2VzTW9kdWxlJywgeyB2YWx1ZTogdHJ1ZSB9KTtcbiBcdH07XG5cbiBcdC8vIGNyZWF0ZSBhIGZha2UgbmFtZXNwYWNlIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDE6IHZhbHVlIGlzIGEgbW9kdWxlIGlkLCByZXF1aXJlIGl0XG4gXHQvLyBtb2RlICYgMjogbWVyZ2UgYWxsIHByb3BlcnRpZXMgb2YgdmFsdWUgaW50byB0aGUgbnNcbiBcdC8vIG1vZGUgJiA0OiByZXR1cm4gdmFsdWUgd2hlbiBhbHJlYWR5IG5zIG9iamVjdFxuIFx0Ly8gbW9kZSAmIDh8MTogYmVoYXZlIGxpa2UgcmVxdWlyZVxuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy50ID0gZnVuY3Rpb24odmFsdWUsIG1vZGUpIHtcbiBcdFx0aWYobW9kZSAmIDEpIHZhbHVlID0gX193ZWJwYWNrX3JlcXVpcmVfXyh2YWx1ZSk7XG4gXHRcdGlmKG1vZGUgJiA4KSByZXR1cm4gdmFsdWU7XG4gXHRcdGlmKChtb2RlICYgNCkgJiYgdHlwZW9mIHZhbHVlID09PSAnb2JqZWN0JyAmJiB2YWx1ZSAmJiB2YWx1ZS5fX2VzTW9kdWxlKSByZXR1cm4gdmFsdWU7XG4gXHRcdHZhciBucyA9IE9iamVjdC5jcmVhdGUobnVsbCk7XG4gXHRcdF9fd2VicGFja19yZXF1aXJlX18ucihucyk7XG4gXHRcdE9iamVjdC5kZWZpbmVQcm9wZXJ0eShucywgJ2RlZmF1bHQnLCB7IGVudW1lcmFibGU6IHRydWUsIHZhbHVlOiB2YWx1ZSB9KTtcbiBcdFx0aWYobW9kZSAmIDIgJiYgdHlwZW9mIHZhbHVlICE9ICdzdHJpbmcnKSBmb3IodmFyIGtleSBpbiB2YWx1ZSkgX193ZWJwYWNrX3JlcXVpcmVfXy5kKG5zLCBrZXksIGZ1bmN0aW9uKGtleSkgeyByZXR1cm4gdmFsdWVba2V5XTsgfS5iaW5kKG51bGwsIGtleSkpO1xuIFx0XHRyZXR1cm4gbnM7XG4gXHR9O1xuXG4gXHQvLyBnZXREZWZhdWx0RXhwb3J0IGZ1bmN0aW9uIGZvciBjb21wYXRpYmlsaXR5IHdpdGggbm9uLWhhcm1vbnkgbW9kdWxlc1xuIFx0X193ZWJwYWNrX3JlcXVpcmVfXy5uID0gZnVuY3Rpb24obW9kdWxlKSB7XG4gXHRcdHZhciBnZXR0ZXIgPSBtb2R1bGUgJiYgbW9kdWxlLl9fZXNNb2R1bGUgP1xuIFx0XHRcdGZ1bmN0aW9uIGdldERlZmF1bHQoKSB7IHJldHVybiBtb2R1bGVbJ2RlZmF1bHQnXTsgfSA6XG4gXHRcdFx0ZnVuY3Rpb24gZ2V0TW9kdWxlRXhwb3J0cygpIHsgcmV0dXJuIG1vZHVsZTsgfTtcbiBcdFx0X193ZWJwYWNrX3JlcXVpcmVfXy5kKGdldHRlciwgJ2EnLCBnZXR0ZXIpO1xuIFx0XHRyZXR1cm4gZ2V0dGVyO1xuIFx0fTtcblxuIFx0Ly8gT2JqZWN0LnByb3RvdHlwZS5oYXNPd25Qcm9wZXJ0eS5jYWxsXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLm8gPSBmdW5jdGlvbihvYmplY3QsIHByb3BlcnR5KSB7IHJldHVybiBPYmplY3QucHJvdG90eXBlLmhhc093blByb3BlcnR5LmNhbGwob2JqZWN0LCBwcm9wZXJ0eSk7IH07XG5cbiBcdC8vIF9fd2VicGFja19wdWJsaWNfcGF0aF9fXG4gXHRfX3dlYnBhY2tfcmVxdWlyZV9fLnAgPSBcIi9fa2FybWFfd2VicGFja18vXCI7XG5cbiBcdHZhciBqc29ucEFycmF5ID0gd2luZG93W1wid2VicGFja0pzb25wXCJdID0gd2luZG93W1wid2VicGFja0pzb25wXCJdIHx8IFtdO1xuIFx0dmFyIG9sZEpzb25wRnVuY3Rpb24gPSBqc29ucEFycmF5LnB1c2guYmluZChqc29ucEFycmF5KTtcbiBcdGpzb25wQXJyYXkucHVzaCA9IHdlYnBhY2tKc29ucENhbGxiYWNrO1xuIFx0anNvbnBBcnJheSA9IGpzb25wQXJyYXkuc2xpY2UoKTtcbiBcdGZvcih2YXIgaSA9IDA7IGkgPCBqc29ucEFycmF5Lmxlbmd0aDsgaSsrKSB3ZWJwYWNrSnNvbnBDYWxsYmFjayhqc29ucEFycmF5W2ldKTtcbiBcdHZhciBwYXJlbnRKc29ucEZ1bmN0aW9uID0gb2xkSnNvbnBGdW5jdGlvbjtcblxuXG4gXHQvLyBhZGQgZW50cnkgbW9kdWxlIHRvIGRlZmVycmVkIGxpc3RcbiBcdGRlZmVycmVkTW9kdWxlcy5wdXNoKFtcIi4vc3JjL3Rlc3QudHNcIixcInZlbmRvclwiXSk7XG4gXHQvLyBydW4gZGVmZXJyZWQgbW9kdWxlcyB3aGVuIHJlYWR5XG4gXHRyZXR1cm4gY2hlY2tEZWZlcnJlZE1vZHVsZXMoKTtcbiIsImZ1bmN0aW9uIHdlYnBhY2tFbXB0eUFzeW5jQ29udGV4dChyZXEpIHtcblx0Ly8gSGVyZSBQcm9taXNlLnJlc29sdmUoKS50aGVuKCkgaXMgdXNlZCBpbnN0ZWFkIG9mIG5ldyBQcm9taXNlKCkgdG8gcHJldmVudFxuXHQvLyB1bmNhdWdodCBleGNlcHRpb24gcG9wcGluZyB1cCBpbiBkZXZ0b29sc1xuXHRyZXR1cm4gUHJvbWlzZS5yZXNvbHZlKCkudGhlbihmdW5jdGlvbigpIHtcblx0XHR2YXIgZSA9IG5ldyBFcnJvcihcIkNhbm5vdCBmaW5kIG1vZHVsZSAnXCIgKyByZXEgKyBcIidcIik7XG5cdFx0ZS5jb2RlID0gJ01PRFVMRV9OT1RfRk9VTkQnO1xuXHRcdHRocm93IGU7XG5cdH0pO1xufVxud2VicGFja0VtcHR5QXN5bmNDb250ZXh0LmtleXMgPSBmdW5jdGlvbigpIHsgcmV0dXJuIFtdOyB9O1xud2VicGFja0VtcHR5QXN5bmNDb250ZXh0LnJlc29sdmUgPSB3ZWJwYWNrRW1wdHlBc3luY0NvbnRleHQ7XG5tb2R1bGUuZXhwb3J0cyA9IHdlYnBhY2tFbXB0eUFzeW5jQ29udGV4dDtcbndlYnBhY2tFbXB0eUFzeW5jQ29udGV4dC5pZCA9IFwiLi8kJF9sYXp5X3JvdXRlX3Jlc291cmNlIGxhenkgcmVjdXJzaXZlXCI7IiwiZXhwb3J0IGRlZmF1bHQgXCI8YXBwLXNlYXJjaC1pbWFnZXM+PC9hcHAtc2VhcmNoLWltYWdlcz5cXG5cIjsiLCJleHBvcnQgZGVmYXVsdCBcIlxcblxcbiAgPGRpdiBzdHlsZT1cXFwidGV4dC1hbGlnbjogY2VudGVyO1xcXCI+XFxuICAgIDxpbnB1dCB0eXBlPVxcXCJzZWFyY2hcXFwiIGNsYXNzPVxcXCJmb3JtLWNvbnRyb2xcXFwiIChrZXl1cCk9XFxcInNlYXJjaCgkZXZlbnQpXFxcIiBwbGFjZWhvbGRlcj1cXFwiU2VhcmNoIGltYWdlc1xcXCIgdmFsdWU9XFxcIkFndWFzY2FsaWVudGVzXFxcIj5cXG4gIDwvZGl2PlxcblxcbiAgPGRpdiBjbGFzcz1cXFwiY2FqYVxcXCI+XFxuICAgIDxkaXYgY2xhc3M9XFxcInJvd1xcXCI+XFxuICAgICAgPGRpdiAqbmdGb3I9XFxcImxldCBpbWFnZW4gb2YgaW1hZ2VuZXNcXFwiIChjbGljayk9XFxcIm9uU2VsZWN0KGltYWdlbilcXFwiPlxcbiAgICAgICAgPGRpdiBjbGFzcz1cXFwiY29sdW1uXFxcIj5cXG4gICAgICAgICAgPGltZyBzcmM9XFxcInt7aW1hZ2VuLnVybH19X20uanBnXFxcIiB0aXRsZT1cXFwie3tpbWFnZW4udGl0bGV9fVxcXCI+ICAgICAgICAgIFxcbiAgICAgICAgICBcXG4gICAgICAgICAgICA8ZGl2IGNsYXNzPVxcXCJ0ZXh0XFxcIiAqbmdJZj1cXFwiaW1hZ2VuLmZhdm9yaXRvXFxcIiA+IFxcbiAgICAgICAgICAgICAgPGgxPlNlbGVjdGVkPC9oMT5cXG4gICAgICAgICAgPC9kaXY+ICAgIFxcbiAgICAgICAgPC9kaXY+XFxuICAgICAgPC9kaXY+XFxuICAgIDwvZGl2PlxcbiAgICA8ZGl2IGNsYXNzPVxcXCJzZWFyY2gtcmVzdWx0c1xcXCIgaW5maW5pdGVTY3JvbGwgW2luZmluaXRlU2Nyb2xsRGlzdGFuY2VdPVxcXCIyXFxcIiBbaW5maW5pdGVTY3JvbGxUaHJvdHRsZV09XFxcIjUwXFxcIlxcbiAgICAgIChzY3JvbGxlZCk9XFxcIm9uU2Nyb2xsKClcXFwiPlxcbiAgICA8L2Rpdj5cXG4gIDwvZGl2PlxcblxcblwiOyIsInZhciBnO1xuXG4vLyBUaGlzIHdvcmtzIGluIG5vbi1zdHJpY3QgbW9kZVxuZyA9IChmdW5jdGlvbigpIHtcblx0cmV0dXJuIHRoaXM7XG59KSgpO1xuXG50cnkge1xuXHQvLyBUaGlzIHdvcmtzIGlmIGV2YWwgaXMgYWxsb3dlZCAoc2VlIENTUClcblx0ZyA9IGcgfHwgbmV3IEZ1bmN0aW9uKFwicmV0dXJuIHRoaXNcIikoKTtcbn0gY2F0Y2ggKGUpIHtcblx0Ly8gVGhpcyB3b3JrcyBpZiB0aGUgd2luZG93IHJlZmVyZW5jZSBpcyBhdmFpbGFibGVcblx0aWYgKHR5cGVvZiB3aW5kb3cgPT09IFwib2JqZWN0XCIpIGcgPSB3aW5kb3c7XG59XG5cbi8vIGcgY2FuIHN0aWxsIGJlIHVuZGVmaW5lZCwgYnV0IG5vdGhpbmcgdG8gZG8gYWJvdXQgaXQuLi5cbi8vIFdlIHJldHVybiB1bmRlZmluZWQsIGluc3RlYWQgb2Ygbm90aGluZyBoZXJlLCBzbyBpdCdzXG4vLyBlYXNpZXIgdG8gaGFuZGxlIHRoaXMgY2FzZS4gaWYoIWdsb2JhbCkgeyAuLi59XG5cbm1vZHVsZS5leHBvcnRzID0gZztcbiIsInZhciBtYXAgPSB7XG5cdFwiLi9hcHAvYXBwLmNvbXBvbmVudC5zcGVjLnRzXCI6IFwiLi9zcmMvYXBwL2FwcC5jb21wb25lbnQuc3BlYy50c1wiLFxuXHRcIi4vYXBwL3NlYXJjaC1pbWFnZXMvc2VhcmNoLWltYWdlcy5jb21wb25lbnQuc3BlYy50c1wiOiBcIi4vc3JjL2FwcC9zZWFyY2gtaW1hZ2VzL3NlYXJjaC1pbWFnZXMuY29tcG9uZW50LnNwZWMudHNcIixcblx0XCIuL2FwcC9zZXJ2aWNlcy9mbGlja3Iuc2VydmljZS5zcGVjLnRzXCI6IFwiLi9zcmMvYXBwL3NlcnZpY2VzL2ZsaWNrci5zZXJ2aWNlLnNwZWMudHNcIlxufTtcblxuXG5mdW5jdGlvbiB3ZWJwYWNrQ29udGV4dChyZXEpIHtcblx0dmFyIGlkID0gd2VicGFja0NvbnRleHRSZXNvbHZlKHJlcSk7XG5cdHJldHVybiBfX3dlYnBhY2tfcmVxdWlyZV9fKGlkKTtcbn1cbmZ1bmN0aW9uIHdlYnBhY2tDb250ZXh0UmVzb2x2ZShyZXEpIHtcblx0aWYoIV9fd2VicGFja19yZXF1aXJlX18ubyhtYXAsIHJlcSkpIHtcblx0XHR2YXIgZSA9IG5ldyBFcnJvcihcIkNhbm5vdCBmaW5kIG1vZHVsZSAnXCIgKyByZXEgKyBcIidcIik7XG5cdFx0ZS5jb2RlID0gJ01PRFVMRV9OT1RfRk9VTkQnO1xuXHRcdHRocm93IGU7XG5cdH1cblx0cmV0dXJuIG1hcFtyZXFdO1xufVxud2VicGFja0NvbnRleHQua2V5cyA9IGZ1bmN0aW9uIHdlYnBhY2tDb250ZXh0S2V5cygpIHtcblx0cmV0dXJuIE9iamVjdC5rZXlzKG1hcCk7XG59O1xud2VicGFja0NvbnRleHQucmVzb2x2ZSA9IHdlYnBhY2tDb250ZXh0UmVzb2x2ZTtcbm1vZHVsZS5leHBvcnRzID0gd2VicGFja0NvbnRleHQ7XG53ZWJwYWNrQ29udGV4dC5pZCA9IFwiLi9zcmMgc3luYyByZWN1cnNpdmUgXFxcXC5zcGVjXFxcXC50cyRcIjsiLCJleHBvcnQgZGVmYXVsdCBcIlxcbi8qIyBzb3VyY2VNYXBwaW5nVVJMPWRhdGE6YXBwbGljYXRpb24vanNvbjtiYXNlNjQsZXlKMlpYSnphVzl1SWpvekxDSnpiM1Z5WTJWeklqcGJYU3dpYm1GdFpYTWlPbHRkTENKdFlYQndhVzVuY3lJNklpSXNJbVpwYkdVaU9pSnpjbU12WVhCd0wyRndjQzVqYjIxd2IyNWxiblF1WTNOekluMD0gKi9cIjsiLCJpbXBvcnQgeyBUZXN0QmVkLCBhc3luYyB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUvdGVzdGluZyc7XG5pbXBvcnQgeyBBcHBDb21wb25lbnQgfSBmcm9tICcuL2FwcC5jb21wb25lbnQnO1xuXG5kZXNjcmliZSgnQXBwQ29tcG9uZW50JywgKCkgPT4ge1xuICBiZWZvcmVFYWNoKGFzeW5jKCgpID0+IHtcbiAgICBUZXN0QmVkLmNvbmZpZ3VyZVRlc3RpbmdNb2R1bGUoe1xuICAgICAgZGVjbGFyYXRpb25zOiBbXG4gICAgICAgIEFwcENvbXBvbmVudFxuICAgICAgXSxcbiAgICB9KS5jb21waWxlQ29tcG9uZW50cygpO1xuICB9KSk7XG5cbiAgaXQoJ0NyZWFjaW9uIGRlbCBjb21wb25lbnRlJywgKCkgPT4ge1xuICAgIGNvbnN0IGZpeHR1cmUgPSBUZXN0QmVkLmNyZWF0ZUNvbXBvbmVudChBcHBDb21wb25lbnQpO1xuICAgIGNvbnN0IGFwcCA9IGZpeHR1cmUuZGVidWdFbGVtZW50LmNvbXBvbmVudEluc3RhbmNlO1xuICAgIGV4cGVjdChhcHApLnRvQmVUcnV0aHkoKTtcbiAgfSk7XG5cbiAgaXQoJ0NyZWFjaW9uIHNvbG8gZGUgZWwgbWFyY28gZGUgdHJhYmFqbycsICgpID0+IHtcbiAgICAvL2JlY2F1c2UgYWxsIHdpbGwgYmUgdXNlZCBpbiB0aGUgbmV4dCBjb21wb25lbnRzXG4gICAgY29uc3QgZml4dHVyZSA9IFRlc3RCZWQuY3JlYXRlQ29tcG9uZW50KEFwcENvbXBvbmVudCk7XG4gICAgY29uc3QgYXBwID0gZml4dHVyZS5kZWJ1Z0VsZW1lbnQuY29tcG9uZW50SW5zdGFuY2U7XG4gICAgZXhwZWN0KGFwcC50aXRsZSkudG9FcXVhbCgnSW5maW5pdGUtc2Nyb2xsJyk7XG5cbiAgfSk7XG5cbiAgXG59KTtcbiIsImltcG9ydCB7IENvbXBvbmVudCB9IGZyb20gJ0Bhbmd1bGFyL2NvcmUnO1xuXG5AQ29tcG9uZW50KHtcbiAgc2VsZWN0b3I6ICdhcHAtcm9vdCcsXG4gIHRlbXBsYXRlVXJsOiAnLi9hcHAuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9hcHAuY29tcG9uZW50LmNzcyddXG59KVxuZXhwb3J0IGNsYXNzIEFwcENvbXBvbmVudCB7XG4gIC8vaW5pY2lhbGl6YSBlbCBub21icmUgZGVsIHByb2plY3RvLiBcbiAgdGl0bGUgPSAnSW5maW5pdGUtc2Nyb2xsJztcbn1cbiIsImV4cG9ydCBkZWZhdWx0IFwiKntcXG4gIGJveC1zaXppbmc6IGJvcmRlci1ib3g7XFxufVxcblxcbi5jb2x1bW57XFxuICBwb3NpdGlvbjogcmVsYXRpdmU7XFxuICBkaXNwbGF5OiBpbmxpbmUtYmxvY2s7IC8qIE1ha2UgdGhlIHdpZHRoIG9mIGJveCBzYW1lIGFzIGltYWdlICovXFxuICBmbG9hdDogbGVmdDtcXG4gIHdpZHRoOiAzMCU7XFxuICBcXG4gIG1hcmdpbjogMXJlbTtcXG59XFxuXFxuLmNvbHVtbiBpbWcge1xcbiAgYm9yZGVyLXJhZGl1czogMjBweDtcXG4gIGJvcmRlcjogM3B4IHNvbGlkIGxpZ2h0Z3JheTtcXG4gXFxufVxcblxcbi5jb2x1bW4gLnRleHR7XFxuICBwb3NpdGlvbjogYWJzb2x1dGU7XFxuICB6LWluZGV4OiA5OTk7XFxuICBtYXJnaW46IDAgYXV0bztcXG4gIGxlZnQ6IDA7XFxuICByaWdodDogMDsgICAgICAgIFxcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgdG9wOiA0MCU7IC8qIEFkanVzdCB0aGlzIHZhbHVlIHRvIG1vdmUgdGhlIHBvc2l0aW9uZWQgZGl2IHVwIGFuZCBkb3duICovXFxuICBiYWNrZ3JvdW5kOiByZ2JhKDAsIDAsIDAsIDAuOCk7XFxuICBmb250LWZhbWlseTogQXJpYWwsc2Fucy1zZXJpZjtcXG4gIGNvbG9yOiAjZmZmO1xcbiAgd2lkdGg6IDYwJTsgLyogU2V0IHRoZSB3aWR0aCBvZiB0aGUgcG9zaXRpb25lZCBkaXYgKi9cXG59XFxuXFxuLnJvdzphZnRlciB7XFxuICBjb250ZW50OiBcXFwiXFxcIjtcXG4gIGRpc3BsYXk6IHRhYmxlO1xcbiAgY2xlYXI6IGJvdGg7XFxufVxcblxcbi5jYWphIHtcXG4gIG1hcmdpbi1sZWZ0OiAxNSU7XFxuICBtYXJnaW4tcmlnaHQ6IDE1JTtcXG4gIGJhY2tncm91bmQtY29sb3I6ICNlN2U3ZTc7XFxuICBwYWRkaW5nOiA1MHB4O1xcbiAgY29sb3I6IGJsYWNrO1xcbiAgdGV4dC1hbGlnbjogY2VudGVyO1xcbiAgZm9udC1zaXplOiAxNHB4O1xcbiAgYm94LXNoYWRvdzogMCAxcHggM3B4IHJnYmEoMCwgMCwgMCwgMC4xMiksIDAgMXB4IDJweCByZ2JhKDAsIDAsIDAsIDAuMjQpO1xcbn1cXG5cXG5pbWcge1xcbiAgLW8tb2JqZWN0LWZpdDogY292ZXI7XFxuICAgICBvYmplY3QtZml0OiBjb3ZlcjtcXG4gIHdpZHRoOiAxMDAlO1xcbiAgaGVpZ2h0OiAyNTBweDtcXG59XFxuXFxuQG1lZGlhIHNjcmVlbiBhbmQgKG1heC13aWR0aDogOTkycHgpIHtcXG4gIC5jb2x1bW4ge1xcbiAgICB3aWR0aDogNTAlO1xcbiAgfVxcbn1cXG5cXG5AbWVkaWEgc2NyZWVuIGFuZCAobWF4LXdpZHRoOiA2MDBweCkge1xcbiAgLmNvbHVtbiB7XFxuICAgIHdpZHRoOiAxMDAlO1xcbiAgfVxcbn1cXG5cXG4vKiMgc291cmNlTWFwcGluZ1VSTD1kYXRhOmFwcGxpY2F0aW9uL2pzb247YmFzZTY0LGV5SjJaWEp6YVc5dUlqb3pMQ0p6YjNWeVkyVnpJanBiSW5OeVl5OWhjSEF2YzJWaGNtTm9MV2x0WVdkbGN5OXpaV0Z5WTJndGFXMWhaMlZ6TG1OdmJYQnZibVZ1ZEM1amMzTWlYU3dpYm1GdFpYTWlPbHRkTENKdFlYQndhVzVuY3lJNklrRkJRVUU3UlVGRFJTeHpRa0ZCYzBJN1FVRkRlRUk3TzBGQlJVRTdSVUZEUlN4clFrRkJhMEk3UlVGRGJFSXNjVUpCUVhGQ0xFVkJRVVVzZDBOQlFYZERPMFZCUXk5RUxGZEJRVmM3UlVGRFdDeFZRVUZWT3p0RlFVVldMRmxCUVZrN1FVRkRaRHM3UVVGRFFUdEZRVU5GTEcxQ1FVRnRRanRGUVVOdVFpd3lRa0ZCTWtJN08wRkJSVGRDT3p0QlFVTkJPMFZCUTBVc2EwSkJRV3RDTzBWQlEyeENMRmxCUVZrN1JVRkRXaXhqUVVGak8wVkJRMlFzVDBGQlR6dEZRVU5RTEZGQlFWRTdSVUZEVWl4clFrRkJhMEk3UlVGRGJFSXNVVUZCVVN4RlFVRkZMRFpFUVVFMlJEdEZRVU4yUlN3NFFrRkJPRUk3UlVGRE9VSXNOa0pCUVRaQ08wVkJRemRDTEZkQlFWYzdSVUZEV0N4VlFVRlZMRVZCUVVVc2QwTkJRWGRETzBGQlEzUkVPenRCUVVWQk8wVkJRMFVzVjBGQlZ6dEZRVU5ZTEdOQlFXTTdSVUZEWkN4WFFVRlhPMEZCUTJJN08wRkJSVUU3UlVGRFJTeG5Ra0ZCWjBJN1JVRkRhRUlzYVVKQlFXbENPMFZCUTJwQ0xIbENRVUY1UWp0RlFVTjZRaXhoUVVGaE8wVkJRMklzV1VGQldUdEZRVU5hTEd0Q1FVRnJRanRGUVVOc1FpeGxRVUZsTzBWQlEyWXNkMFZCUVhkRk8wRkJRekZGT3p0QlFVVkJPMFZCUTBVc2IwSkJRV2xDTzB0QlFXcENMR2xDUVVGcFFqdEZRVU5xUWl4WFFVRlhPMFZCUTFnc1lVRkJZVHRCUVVObU96dEJRVVZCTzBWQlEwVTdTVUZEUlN4VlFVRlZPMFZCUTFvN1FVRkRSanM3UVVGRlFUdEZRVU5GTzBsQlEwVXNWMEZCVnp0RlFVTmlPMEZCUTBZaUxDSm1hV3hsSWpvaWMzSmpMMkZ3Y0M5elpXRnlZMmd0YVcxaFoyVnpMM05sWVhKamFDMXBiV0ZuWlhNdVkyOXRjRzl1Wlc1MExtTnpjeUlzSW5OdmRYSmpaWE5EYjI1MFpXNTBJanBiSWlwN1hHNGdJR0p2ZUMxemFYcHBibWM2SUdKdmNtUmxjaTFpYjNnN1hHNTlYRzVjYmk1amIyeDFiVzU3WEc0Z0lIQnZjMmwwYVc5dU9pQnlaV3hoZEdsMlpUdGNiaUFnWkdsemNHeGhlVG9nYVc1c2FXNWxMV0pzYjJOck95QXZLaUJOWVd0bElIUm9aU0IzYVdSMGFDQnZaaUJpYjNnZ2MyRnRaU0JoY3lCcGJXRm5aU0FxTDF4dUlDQm1iRzloZERvZ2JHVm1kRHRjYmlBZ2QybGtkR2c2SURNd0pUdGNiaUFnWEc0Z0lHMWhjbWRwYmpvZ01YSmxiVHRjYm4xY2JpNWpiMngxYlc0Z2FXMW5JSHRjYmlBZ1ltOXlaR1Z5TFhKaFpHbDFjem9nTWpCd2VEdGNiaUFnWW05eVpHVnlPaUF6Y0hnZ2MyOXNhV1FnYkdsbmFIUm5jbUY1TzF4dUlGeHVmVnh1TG1OdmJIVnRiaUF1ZEdWNGRIdGNiaUFnY0c5emFYUnBiMjQ2SUdGaWMyOXNkWFJsTzF4dUlDQjZMV2x1WkdWNE9pQTVPVGs3WEc0Z0lHMWhjbWRwYmpvZ01DQmhkWFJ2TzF4dUlDQnNaV1owT2lBd08xeHVJQ0J5YVdkb2REb2dNRHNnSUNBZ0lDQWdJRnh1SUNCMFpYaDBMV0ZzYVdkdU9pQmpaVzUwWlhJN1hHNGdJSFJ2Y0RvZ05EQWxPeUF2S2lCQlpHcDFjM1FnZEdocGN5QjJZV3gxWlNCMGJ5QnRiM1psSUhSb1pTQndiM05wZEdsdmJtVmtJR1JwZGlCMWNDQmhibVFnWkc5M2JpQXFMMXh1SUNCaVlXTnJaM0p2ZFc1a09pQnlaMkpoS0RBc0lEQXNJREFzSURBdU9DazdYRzRnSUdadmJuUXRabUZ0YVd4NU9pQkJjbWxoYkN4ellXNXpMWE5sY21sbU8xeHVJQ0JqYjJ4dmNqb2dJMlptWmp0Y2JpQWdkMmxrZEdnNklEWXdKVHNnTHlvZ1UyVjBJSFJvWlNCM2FXUjBhQ0J2WmlCMGFHVWdjRzl6YVhScGIyNWxaQ0JrYVhZZ0tpOWNibjFjYmx4dUxuSnZkenBoWm5SbGNpQjdYRzRnSUdOdmJuUmxiblE2SUZ3aVhDSTdYRzRnSUdScGMzQnNZWGs2SUhSaFlteGxPMXh1SUNCamJHVmhjam9nWW05MGFEdGNibjFjYmx4dUxtTmhhbUVnZTF4dUlDQnRZWEpuYVc0dGJHVm1kRG9nTVRVbE8xeHVJQ0J0WVhKbmFXNHRjbWxuYUhRNklERTFKVHRjYmlBZ1ltRmphMmR5YjNWdVpDMWpiMnh2Y2pvZ0kyVTNaVGRsTnp0Y2JpQWdjR0ZrWkdsdVp6b2dOVEJ3ZUR0Y2JpQWdZMjlzYjNJNklHSnNZV05yTzF4dUlDQjBaWGgwTFdGc2FXZHVPaUJqWlc1MFpYSTdYRzRnSUdadmJuUXRjMmw2WlRvZ01UUndlRHRjYmlBZ1ltOTRMWE5vWVdSdmR6b2dNQ0F4Y0hnZ00zQjRJSEpuWW1Fb01Dd2dNQ3dnTUN3Z01DNHhNaWtzSURBZ01YQjRJREp3ZUNCeVoySmhLREFzSURBc0lEQXNJREF1TWpRcE8xeHVmVnh1WEc1cGJXY2dlMXh1SUNCdlltcGxZM1F0Wm1sME9pQmpiM1psY2p0Y2JpQWdkMmxrZEdnNklERXdNQ1U3WEc0Z0lHaGxhV2RvZERvZ01qVXdjSGc3WEc1OVhHNWNia0J0WldScFlTQnpZM0psWlc0Z1lXNWtJQ2h0WVhndGQybGtkR2c2SURrNU1uQjRLU0I3WEc0Z0lDNWpiMngxYlc0Z2UxeHVJQ0FnSUhkcFpIUm9PaUExTUNVN1hHNGdJSDFjYm4xY2JseHVRRzFsWkdsaElITmpjbVZsYmlCaGJtUWdLRzFoZUMxM2FXUjBhRG9nTmpBd2NIZ3BJSHRjYmlBZ0xtTnZiSFZ0YmlCN1hHNGdJQ0FnZDJsa2RHZzZJREV3TUNVN1hHNGdJSDFjYm4xY2JpSmRmUT09ICovXCI7IiwiaW1wb3J0IHsgYXN5bmMsIENvbXBvbmVudEZpeHR1cmUsIFRlc3RCZWQgfSBmcm9tICdAYW5ndWxhci9jb3JlL3Rlc3RpbmcnO1xuXG5pbXBvcnQgeyBTZWFyY2hJbWFnZXNDb21wb25lbnQgfSBmcm9tICcuL3NlYXJjaC1pbWFnZXMuY29tcG9uZW50JztcbmltcG9ydCB7IEh0dHBDbGllbnRUZXN0aW5nTW9kdWxlIH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAvdGVzdGluZydcblxuZGVzY3JpYmUoJ1NlYXJjaEltYWdlc0NvbXBvbmVudCcsICgpID0+IHtcbiAgbGV0IGNvbXBvbmVudDogU2VhcmNoSW1hZ2VzQ29tcG9uZW50O1xuICBsZXQgZml4dHVyZTogQ29tcG9uZW50Rml4dHVyZTxTZWFyY2hJbWFnZXNDb21wb25lbnQ+O1xuXG4gIGJlZm9yZUVhY2goYXN5bmMoKCkgPT4ge1xuICAgIFRlc3RCZWQuY29uZmlndXJlVGVzdGluZ01vZHVsZSh7XG4gICAgICBpbXBvcnRzOiBbSHR0cENsaWVudFRlc3RpbmdNb2R1bGVdXG4gICAgICAgLFxuICAgICAgZGVjbGFyYXRpb25zOiBbIFNlYXJjaEltYWdlc0NvbXBvbmVudCBdXG4gICAgfSlcbiAgICAuY29tcGlsZUNvbXBvbmVudHMoKTtcbiAgfSkpO1xuXG4gIGJlZm9yZUVhY2goKCkgPT4ge1xuICAgIGZpeHR1cmUgPSBUZXN0QmVkLmNyZWF0ZUNvbXBvbmVudChTZWFyY2hJbWFnZXNDb21wb25lbnQpO1xuICAgIGNvbXBvbmVudCA9IGZpeHR1cmUuY29tcG9uZW50SW5zdGFuY2U7XG4gICAgZml4dHVyZS5kZXRlY3RDaGFuZ2VzKCk7XG4gIH0pO1xuXG4vLyB2ZXJpZmljYSBxdWUgc2UgcHVlZGEgY3JlYXIgbG9zIGNvbXBvbmVudGVzLiBcbiAgaXQoJ0NvbnBvbmVudGUgY3JlYWRvIHkgdmFsaWRhZG8nLCAoKSA9PiB7XG4gICAgZXhwZWN0KGNvbXBvbmVudCkudG9CZVRydXRoeSgpO1xuICB9KTtcbn0pO1xuIiwiaW1wb3J0IHsgQ29tcG9uZW50LCBPbkluaXQgfSBmcm9tICdAYW5ndWxhci9jb3JlJztcbmltcG9ydCB7IEZsaWNrclNlcnZpY2UgfSBmcm9tICcuLi9zZXJ2aWNlcy9mbGlja3Iuc2VydmljZSc7XG5cbkBDb21wb25lbnQoe1xuICBzZWxlY3RvcjogJ2FwcC1zZWFyY2gtaW1hZ2VzJyxcbiAgdGVtcGxhdGVVcmw6ICcuL3NlYXJjaC1pbWFnZXMuY29tcG9uZW50Lmh0bWwnLFxuICBzdHlsZVVybHM6IFsnLi9zZWFyY2gtaW1hZ2VzLmNvbXBvbmVudC5jc3MnXVxufSlcbmV4cG9ydCBjbGFzcyBTZWFyY2hJbWFnZXNDb21wb25lbnQgaW1wbGVtZW50cyBPbkluaXQge1xuICBpbWFnZW5lcyA9IFtdO1xuICBmYXZvcml0ZXMgPSBbXTtcbiAga2V5d29yZDogc3RyaW5nID1cIkFndWFzY2FsaWVudGVzXCI7XG5cbiAgY29uc3RydWN0b3IocHJpdmF0ZSBmbGlja3JTZXJ2aWNlOiBGbGlja3JTZXJ2aWNlKSB7XG4gICAgLy8gdG8gc2hvdyB1cCBBZ3Vhc2NhbGllbnRlc1xuICAgIHRoaXMuZmxpY2tyU2VydmljZS5zZWFyY2hfa2V5d29yZCh0aGlzLmtleXdvcmQpXG4gICAgLnRvUHJvbWlzZSgpXG4gICAgLnRoZW4ocmVzID0+IHtcbiAgICAgIHRoaXMuaW1hZ2VuZXMgPSByZXM7XG4gICAgfSk7XG4gIH1cblxuICBuZ09uSW5pdCgpIHtcbiAgIFxuICB9XG5cbiAgc2VhcmNoKGV2ZW50OiBhbnkpIHtcbiAgICB0aGlzLmtleXdvcmQgPSBldmVudC50YXJnZXQudmFsdWUudG9Mb3dlckNhc2UoKTtcbiAgICBpZiAodGhpcy5rZXl3b3JkICYmIHRoaXMua2V5d29yZC5sZW5ndGggPiAwKSB7XG4gICAgICB0aGlzLmZsaWNrclNlcnZpY2Uuc2VhcmNoX2tleXdvcmQodGhpcy5rZXl3b3JkKVxuICAgICAgICAudG9Qcm9taXNlKClcbiAgICAgICAgLnRoZW4ocmVzID0+IHtcbiAgICAgICAgICB0aGlzLmltYWdlbmVzID0gcmVzO1xuICAgICAgICB9KTtcbiAgICB9XG4gIH1cblxuICBvblNjcm9sbCgpIHtcbiAgICBpZiAodGhpcy5rZXl3b3JkICYmIHRoaXMua2V5d29yZC5sZW5ndGggPiAwKSB7XG4gICAgICB0aGlzLmZsaWNrclNlcnZpY2Uuc2VhcmNoX2tleXdvcmQodGhpcy5rZXl3b3JkKVxuICAgICAgICAudG9Qcm9taXNlKClcbiAgICAgICAgLnRoZW4ocmVzID0+IHtcbiAgICAgICAgICB0aGlzLmltYWdlbmVzID0gdGhpcy5pbWFnZW5lcy5jb25jYXQocmVzKTtcbiAgICAgICAgfSk7XG4gICAgfVxuXG4gIH1cblxuICBvblNlbGVjdChpbWFnZW46IGFueSk6IHZvaWQge1xuICAgIGltYWdlbi5mYXZvcml0byA9ICFpbWFnZW4uZmF2b3JpdG87ICAgIFxuXG4gICAgLy90byBrZWVwIGluIG1lbW9yeSB0aGUgY3VycmVudCBzdGF0ZVxuICAgIGxvY2FsU3RvcmFnZS5zZXRJdGVtKCdmYXZvcml0b3MnK2ltYWdlbi5pZCxpbWFnZW4uZmF2b3JpdG8pO1xuICB9XG5cbiAgcmVhZExvY2FsU3RvcmFnZVZhbHVlKGtleSkge1xuICAgIHJldHVybiBsb2NhbFN0b3JhZ2UuZ2V0SXRlbShrZXkpO1xuICB9XG5cbn1cbiIsImltcG9ydCB7IFRlc3RCZWQgfSBmcm9tICdAYW5ndWxhci9jb3JlL3Rlc3RpbmcnO1xuXG5pbXBvcnQgeyBGbGlja3JTZXJ2aWNlIH0gZnJvbSAnLi9mbGlja3Iuc2VydmljZSc7XG5pbXBvcnQgeyBIdHRwQ2xpZW50VGVzdGluZ01vZHVsZSB9IGZyb20gJ0Bhbmd1bGFyL2NvbW1vbi9odHRwL3Rlc3RpbmcnXG5cbmRlc2NyaWJlKCdGbGlja3JTZXJ2aWNlJywgKCkgPT4ge1xuICBiZWZvcmVFYWNoKCgpID0+IFRlc3RCZWQuY29uZmlndXJlVGVzdGluZ01vZHVsZSh7XG4gICAgaW1wb3J0czogW0h0dHBDbGllbnRUZXN0aW5nTW9kdWxlXVxuXG4gIH0pKTtcblxuICAvL3Byb2JhbmRvIGNvbmV4aW9uIGEgZmxpY2tlciB5IHBhcmFtYW1ldHJvcyBxdWUgcHVlZGFuIG9idGVuZXIgZGF0b3NcbiAgaXQoJ0NyZWFjaW9uIGV4aXRvc2EgeSB2ZXJpZmljYSBjb25leGlvbiBodHRwIGFsIHdlYnNlcnZpY2UnLCAoKSA9PiB7XG4gICAgY29uc3Qgc2VydmljZTogRmxpY2tyU2VydmljZSA9IFRlc3RCZWQuZ2V0KEZsaWNrclNlcnZpY2UpO1xuICAgIGV4cGVjdChzZXJ2aWNlKS50b0JlVHJ1dGh5KCk7XG4gIH0pO1xufSk7XG4iLCJpbXBvcnQgeyBJbmplY3RhYmxlLCBTeXN0ZW1Kc05nTW9kdWxlTG9hZGVyIH0gZnJvbSAnQGFuZ3VsYXIvY29yZSc7XG5pbXBvcnQgeyBIdHRwQ2xpZW50IH0gZnJvbSAnQGFuZ3VsYXIvY29tbW9uL2h0dHAnO1xuaW1wb3J0IHsgbWFwIH0gZnJvbSAncnhqcy9vcGVyYXRvcnMnO1xuaW1wb3J0IHsgZW52aXJvbm1lbnQgfSBmcm9tICdzcmMvZW52aXJvbm1lbnRzL2Vudmlyb25tZW50JztcblxuZXhwb3J0IGludGVyZmFjZSBGbGlja3JQaG90byB7XG4gIGZhcm06IHN0cmluZztcbiAgaWQ6IHN0cmluZztcbiAgc2VjcmV0OiBzdHJpbmc7XG4gIHNlcnZlcjogc3RyaW5nO1xuICB0aXRsZTogc3RyaW5nO1xufVxuXG5leHBvcnQgaW50ZXJmYWNlIEZsaWNrck91dHB1dCB7XG4gIHBob3Rvczoge1xuICAgIHBob3RvOiBGbGlja3JQaG90b1tdO1xuICB9O1xufVxuXG5ASW5qZWN0YWJsZSh7XG4gIHByb3ZpZGVkSW46ICdyb290J1xufSlcbmV4cG9ydCBjbGFzcyBGbGlja3JTZXJ2aWNlIHtcbiAgcHJldktleXdvcmQ6IHN0cmluZztcbiAgY3VyclBhZ2UgPSAxO1xuXG4gIGNvbnN0cnVjdG9yKHByaXZhdGUgaHR0cDogSHR0cENsaWVudCkgeyB9XG5cbiAgc2VhcmNoX2tleXdvcmQoa2V5d29yZDogc3RyaW5nKSB7XG4gICAgaWYgKHRoaXMucHJldktleXdvcmQgPT09IGtleXdvcmQpIHtcbiAgICAgIHRoaXMuY3VyclBhZ2UrKztcbiAgICB9IGVsc2Uge1xuICAgICAgdGhpcy5jdXJyUGFnZSA9IDE7XG4gICAgfVxuICAgIHRoaXMucHJldktleXdvcmQgPSBrZXl3b3JkO1xuICAgIGNvbnN0IHVybCA9ICdodHRwczovL3d3dy5mbGlja3IuY29tL3NlcnZpY2VzL3Jlc3QvP21ldGhvZD1mbGlja3IucGhvdG9zLnNlYXJjaCYnO1xuICAgIGNvbnN0IHBhcmFtcyA9IGBhcGlfa2V5PSR7ZW52aXJvbm1lbnQuZmxpY2tyLmtleX0mdGV4dD0ke2tleXdvcmR9JmZvcm1hdD1qc29uJm5vanNvbmNhbGxiYWNrPTEmcGVyX3BhZ2U9MTImcGFnZT0ke3RoaXMuY3VyclBhZ2V9YDtcblxuICAgIHJldHVybiB0aGlzLmh0dHAuZ2V0KHVybCArIHBhcmFtcykucGlwZShtYXAoKHJlczogRmxpY2tyT3V0cHV0KSA9PiB7XG4gICAgICBjb25zdCB1cmxBcnIgPSBbXTtcbiAgICAgIGNvbnNvbGUubG9nKHVybCtwYXJhbXMpO1xuICAgICAgcmVzLnBob3Rvcy5waG90by5mb3JFYWNoKChwaDogRmxpY2tyUGhvdG8pID0+IHtcbi8vIFJlY292ZXJ5IHRoZSBsYXN0IHN0YWdlIGluIG1lbW9yeSAgZnJvbSBmYXZvcml0b3NcbiAgICAgIGxldCBmYXZvcml0b1ZhciA9IGxvY2FsU3RvcmFnZS5nZXRJdGVtKCdmYXZvcml0b3MnK3BoLmlkKT9sb2NhbFN0b3JhZ2UuZ2V0SXRlbSgnZmF2b3JpdG9zJytwaC5pZCk6ZmFsc2U7XG4vL2NvbnNvbGUubG9nIChmYXZvcml0b1Zhcik7IG1hbnVhbCB0ZXN0aW5nIGRvbmVcbiAgICAgICAgY29uc3QgcGhvdG9PYmogPSB7XG4gICAgICAgICAgaWQgOiBwaC5pZCwgLy90byBtYWtlIGZhdm9yaXRvcyBjb250cm9sIFxuICAgICAgICAgIHVybDogYGh0dHBzOi8vZmFybSR7cGguZmFybX0uc3RhdGljZmxpY2tyLmNvbS8ke3BoLnNlcnZlcn0vJHtwaC5pZH1fJHtwaC5zZWNyZXR9YCxcbiAgICAgICAgICB0aXRsZTogcGgudGl0bGUsXG4gICAgICAgICAgZmF2b3JpdG86ZmF2b3JpdG9WYXJcbiAgICAgICAgfTtcbiAgICAgICAgdXJsQXJyLnB1c2gocGhvdG9PYmopO1xuICAgICAgfSk7XG4gICAgICByZXR1cm4gdXJsQXJyO1xuICAgIH0pKTtcbiAgfVxufVxuIiwiLy8gVGhpcyBmaWxlIGNhbiBiZSByZXBsYWNlZCBkdXJpbmcgYnVpbGQgYnkgdXNpbmcgdGhlIGBmaWxlUmVwbGFjZW1lbnRzYCBhcnJheS5cbi8vIGBuZyBidWlsZCAtLXByb2RgIHJlcGxhY2VzIGBlbnZpcm9ubWVudC50c2Agd2l0aCBgZW52aXJvbm1lbnQucHJvZC50c2AuXG4vLyBUaGUgbGlzdCBvZiBmaWxlIHJlcGxhY2VtZW50cyBjYW4gYmUgZm91bmQgaW4gYGFuZ3VsYXIuanNvbmAuXG5cbmV4cG9ydCBjb25zdCBlbnZpcm9ubWVudCA9IHtcbiAgcHJvZHVjdGlvbjogZmFsc2UsXG4gIGZsaWNrcjoge1xuICAgIGtleTogJzVhODVhYThkMzY5NTA1ZDZiZWZhNTc5ODVkZGZhNjg2J1xuICB9XG59O1xuXG4vKlxuICogRm9yIGVhc2llciBkZWJ1Z2dpbmcgaW4gZGV2ZWxvcG1lbnQgbW9kZSwgeW91IGNhbiBpbXBvcnQgdGhlIGZvbGxvd2luZyBmaWxlXG4gKiB0byBpZ25vcmUgem9uZSByZWxhdGVkIGVycm9yIHN0YWNrIGZyYW1lcyBzdWNoIGFzIGB6b25lLnJ1bmAsIGB6b25lRGVsZWdhdGUuaW52b2tlVGFza2AuXG4gKlxuICogVGhpcyBpbXBvcnQgc2hvdWxkIGJlIGNvbW1lbnRlZCBvdXQgaW4gcHJvZHVjdGlvbiBtb2RlIGJlY2F1c2UgaXQgd2lsbCBoYXZlIGEgbmVnYXRpdmUgaW1wYWN0XG4gKiBvbiBwZXJmb3JtYW5jZSBpZiBhbiBlcnJvciBpcyB0aHJvd24uXG4gKi9cbi8vIGltcG9ydCAnem9uZS5qcy9kaXN0L3pvbmUtZXJyb3InOyAgLy8gSW5jbHVkZWQgd2l0aCBBbmd1bGFyIENMSS5cbiIsIi8vIFRoaXMgZmlsZSBpcyByZXF1aXJlZCBieSBrYXJtYS5jb25mLmpzIGFuZCBsb2FkcyByZWN1cnNpdmVseSBhbGwgdGhlIC5zcGVjIGFuZCBmcmFtZXdvcmsgZmlsZXNcblxuaW1wb3J0ICd6b25lLmpzL2Rpc3Qvem9uZS10ZXN0aW5nJztcbmltcG9ydCB7IGdldFRlc3RCZWQgfSBmcm9tICdAYW5ndWxhci9jb3JlL3Rlc3RpbmcnO1xuaW1wb3J0IHtcbiAgQnJvd3NlckR5bmFtaWNUZXN0aW5nTW9kdWxlLFxuICBwbGF0Zm9ybUJyb3dzZXJEeW5hbWljVGVzdGluZ1xufSBmcm9tICdAYW5ndWxhci9wbGF0Zm9ybS1icm93c2VyLWR5bmFtaWMvdGVzdGluZyc7XG5cbmRlY2xhcmUgY29uc3QgcmVxdWlyZTogYW55O1xuXG4vLyBGaXJzdCwgaW5pdGlhbGl6ZSB0aGUgQW5ndWxhciB0ZXN0aW5nIGVudmlyb25tZW50LlxuZ2V0VGVzdEJlZCgpLmluaXRUZXN0RW52aXJvbm1lbnQoXG4gIEJyb3dzZXJEeW5hbWljVGVzdGluZ01vZHVsZSxcbiAgcGxhdGZvcm1Ccm93c2VyRHluYW1pY1Rlc3RpbmcoKVxuKTtcbi8vIFRoZW4gd2UgZmluZCBhbGwgdGhlIHRlc3RzLlxuY29uc3QgY29udGV4dCA9IHJlcXVpcmUuY29udGV4dCgnLi8nLCB0cnVlLCAvXFwuc3BlY1xcLnRzJC8pO1xuLy8gQW5kIGxvYWQgdGhlIG1vZHVsZXMuXG5jb250ZXh0LmtleXMoKS5tYXAoY29udGV4dCk7XG4iXSwic291cmNlUm9vdCI6IiJ9